export interface Relationship {
	blocked_by: boolean;
	blocking: boolean;
	domain_blocking: boolean;
	endorsed: boolean;
	followed_by: boolean;
	following: boolean;
	id: string;
	muting: boolean;
	muting_notifications: boolean;
	note: string;
	notifying: boolean;
	requested: boolean;
	showing_reblogs: boolean;
	subscribing: boolean;
}
