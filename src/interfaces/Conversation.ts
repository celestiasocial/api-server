import { Account } from "./Account.ts";
import { Status } from "./Status.ts";


export interface Conversation {
	id: string;               // The ID of the conversation in the database
	unread: boolean;          // Is the conversation currently marked as unread?
	accounts: Account[];      // Participants in the conversation
	last_status: Status | null; // The last status in the conversation
}
