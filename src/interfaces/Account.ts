import { CustomEmoji } from './CustomEmoji.ts';
import { Field } from './Field.ts';

export interface Account {
	id: string; // The account id
	username: string; // The username of the account, not including domain
	acct: string; // The Webfinger account URI
	url: string; // The location of the user’s profile page
	display_name: string; // The profile’s display name
	note: string; // The profile’s bio or description
	avatar: string; // An image icon that is shown next to statuses and in the profile
	avatar_static: string; // A static version of the avatar
	header: string; // An image banner that is shown above the profile and in profile cards
	header_static: string; // A static version of the header
	locked: boolean; // Whether the account manually approves follow requests
	fields: Field[]; // Additional metadata attached to a profile as name-value pairs
	emojis: CustomEmoji[]; // Custom emoji entities to be used when rendering the profile
	bot: boolean; // Indicates that the account may perform automated actions
	group: boolean; // Indicates that the account represents a Group actor
	discoverable: boolean | null; // Whether the account has opted into discovery features
	noindex?: boolean | null; // Whether the local user has opted out of being indexed by search engines
	moved?: Account | null; // Indicates that the profile is currently inactive and that its user has moved to a new account
	suspended?: boolean; // An extra attribute returned only when an account is suspended
	limited?: boolean; // An extra attribute returned only when an account is silenced
	created_at: string; // When the account was created
	last_status_at: string | null; // When the most recent status was posted
	statuses_count: number; // How many statuses are attached to this account
	followers_count: number; // The reported followers of this profile
	following_count: number; // The reported follows of this profile
}
