export interface CustomEmoji {
	shortcode: string;          // The name of the custom emoji
	url: string;                // A link to the custom emoji
	static_url: string;         // A link to a static copy of the custom emoji
	visible_in_picker: boolean; // Whether this Emoji should be visible in the picker or unlisted
	category: string;           // Used for sorting custom emoji in the picker
}
