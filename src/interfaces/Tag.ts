interface TagHistory {
	day: string;         // UNIX timestamp on midnight of the given day
	uses: string;        // The counted usage of the tag within that day
	accounts: string;    // The total of accounts using the tag within that day
}

export interface Tag {
	name: string;        // The value of the hashtag after the # sign
	url: string;         // A link to the hashtag on the instance
	history: TagHistory[];  // Usage statistics for given days
	following?: boolean; // Whether the current token’s authorized user is following this tag
}
