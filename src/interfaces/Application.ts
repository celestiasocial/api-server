export interface Application {
	name: string;              // The name of your application
	website?: string | null;   // The website associated with your application
	vapid_key: string;         // Used for Push Streaming API
	client_id?: string;        // Client ID key, to be used for obtaining OAuth tokens
	client_secret?: string;    // Client secret key, to be used for obtaining OAuth tokens
}
