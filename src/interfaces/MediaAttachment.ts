interface Meta {
	// Metadata returned by Paperclip.
	// You might need to expand on this based on what the specific sub-properties and top-level properties are.
	small?: any;        // Placeholder, replace with actual type structure
	original?: any;     // Placeholder, replace with actual type structure
	focus?: {
		x: number;       // Assuming x and y coordinates for focus
		y: number;
	};
	// ... other potential properties
}

export interface MediaAttachment {
	id: string;               // The ID of the attachment in the database
	type: 'unknown' | 'image' | 'gifv' | 'video' | 'audio'; // The type of the attachment
	url: string;              // The location of the original full-size attachment
	preview_url: string;      // The location of a scaled-down preview of the attachment
	remote_url: string | null; // The location of the full-size original attachment on the remote website
	meta: Meta;               // Metadata returned by Paperclip
	description: string;      // Alternate text that describes what is in the media attachment
	blurhash: string;         // A hash computed by the BlurHash algorithm
	// text_url: string;      // Deprecated: A shorter URL for the attachment
}
