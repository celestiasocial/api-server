import { Account } from "./Account.ts";
import { Application } from './Application.ts';
import { CustomEmoji } from "./CustomEmoji.ts";
import { MediaAttachment } from "./MediaAttachment.ts";
import { Poll } from "./Poll.ts";

interface StatusMention {
	id: string;                // The account ID of the mentioned user
	username: string;          // The username of the mentioned user
	url: string;               // The location of the mentioned user’s profile
	acct: string;              // The webfinger acct: URI of the mentioned user
}

interface StatusTag {
	name: string;              // The value of the hashtag after the # sign
	url: string;               // A link to the hashtag on the instance
}

interface PreviewCard {
	// Define the structure of PreviewCard
}

interface FilterResult {
	// Define the structure of FilterResult
}

export interface Status {
	id: string;
	uri: string;
	created_at: string;
	account: Account;
	content: string;
	visibility: 'public' | 'unlisted' | 'private' | 'direct';
	sensitive: boolean;
	spoiler_text: string;
	media_attachments: MediaAttachment[];
	application?: Application;
	mentions: StatusMention[];
	tags: StatusTag[];
	emojis: CustomEmoji[];
	reblogs_count: number;
	favourites_count: number;
	replies_count: number;
	url: string | null;
	in_reply_to_id: string | null;
	in_reply_to_account_id: string | null;
	reblog: Status | null;
	poll: Poll | null;
	card: PreviewCard | null;
	language: string | null;
	text: string | null;
	edited_at: string | null;
	favourited?: boolean;
	reblogged?: boolean;
	muted?: boolean;
	bookmarked?: boolean;
	pinned?: boolean;
	filtered?: FilterResult[];
}
