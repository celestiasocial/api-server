import { CustomEmoji } from './CustomEmoji.ts';

interface PollOption {
	title: string;             // The text value of the poll option
	votes_count: number | null; // The total number of received votes for this option
}

export interface Poll {
	id: string;                 // The ID of the poll in the database
	expires_at: string | null;  // When the poll ends
	expired: boolean;           // Is the poll currently expired?
	multiple: boolean;          // Does the poll allow multiple-choice answers?
	votes_count: number;        // How many votes have been received
	voters_count: number | null; // How many unique accounts have voted on a multiple-choice poll
	options: PollOption[];      // Possible answers for the poll
	emojis: CustomEmoji[];      // Custom emoji to be used for rendering poll options
	voted?: boolean;            // When called with a user token, has the authorized user voted?
	own_votes?: number[];       // When called with a user token, which options has the authorized user chosen?
}
