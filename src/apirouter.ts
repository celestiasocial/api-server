import { Context, Hono } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import {
	registerAccountRoute,
	updateCredentialsRoute,
} from './routes/mastoapi/account-credentials-router.ts';
import { get_account_by_id_route, get_account_relationship } from './routes/mastoapi/account-actions-router.ts';
import { home_timeline_route, public_timeline_route } from './routes/mastoapi/timeline-router.ts';
import { manifest_route } from './routes/mastoapi/misc-router.ts';
import {
	bookmark_status_route,
	create_status_route,
	delete_status_route,
	favourite_status_route,
	get_status_route,
	status_context_route,
} from './routes/mastoapi/status-router.ts';
import { cast_vote_route, get_poll_route } from './routes/mastoapi/polls-router.ts';
import {
	oauth_authorize_form_route,
	oauth_authorize_route,
	oauth_token_route,
} from './routes/mastoapi/oauth-router.ts';
import { upload_media_route } from './routes/mastoapi/media-router.ts';
import {
	account_lookup_route,
	get_statuses_by_account_id_route,
} from './routes/mastoapi/account-information-router.ts';
import { dismiss_announcement_route, get_announcements_route } from './routes/mastoapi/announcements-router.ts';
import { verify_credentials_route } from './routes/mastoapi/verify_credentials.ts';
import { app_router_post } from './routes/mastoapi/app-router.ts';
import { hostMeta } from './routes/activitypub/host-meta.ts';
import { nodeinfo } from './routes/activitypub/nodeinfo.ts';
import { webfinger } from './routes/activitypub/webfinger.ts';
import { generate_instance_info } from './routes/activitypub/generate-instance-info.ts';
import { generateNodeInfoResponse } from './routes/activitypub/generate-node-info-response.ts';
import { handleUserRequest } from './routes/activitypub/handle-user-request.ts';
import { handleOutboxRequest } from './routes/activitypub/handle-outbox-request.ts';
import { handleInboxRequest } from './routes/activitypub/handle-inbox-request.ts';
import { handleFollowersRequest } from './routes/activitypub/handle-followers-request.ts';
import { handleFollowingRequest } from './routes/activitypub/handle-following-request.ts';
import { notImplementedResponse } from './routes/notImplementedResponse.ts';
import { handleInboxPostRequest } from './routes/activitypub/inbox-router.ts';
import { endorse_account_route } from './routes/mastoapi/endorse-account-route.ts';
import { mute_account_route } from './routes/mastoapi/mute-account-route.ts';
import { block_account_route } from './routes/mastoapi/block-account-route.ts';

// Initialize the router
const router = new Hono();

// Setup routes
router.get('/api/v1/instance', generate_instance_info);
router.get('/.well-known/webfinger', webfinger);
router.get('/.well-known/host-meta', hostMeta);
router.get('/.well-known/nodeinfo', nodeinfo);
router.get('/nodeinfo/2.0', (c: Context) => generateNodeInfoResponse(c, "2.0"));
router.get('/nodeinfo/2.1.json', (c: Context) => generateNodeInfoResponse(c, "2.1"));

router.post('/inbox', handleInboxPostRequest);
router.get('/users/:username', handleUserRequest);
router.get('/users/:username/outbox', handleOutboxRequest);
router.get('/users/:username/inbox', handleInboxRequest);
router.get('/users/:username/followers', handleFollowersRequest);
router.get('/users/:username/following', handleFollowingRequest);

router.post('/api/v1/apps', app_router_post);
router.post('/oauth/token', oauth_token_route);
router.get('/oauth/authorize', oauth_authorize_form_route);
router.post('/oauth/authorize', oauth_authorize_route);

router.post('/api/v1/media', upload_media_route);

router.get('/api/v1/announcements', get_announcements_route);
router.post('/api/v1/announcements/:id/dismiss', dismiss_announcement_route);

router.post('/api/v1/accounts', registerAccountRoute);
router.post('/api/v1/follows', notImplementedResponse);
router.get('/api/v1/accounts/verify_credentials', verify_credentials_route);
router.patch('/api/v1/accounts/update_credentials', updateCredentialsRoute);
router.get('/api/v1/accounts/relationships', get_account_relationship);
router.get('/api/v1/accounts/search',);
router.get('/api/v1/accounts/lookup', account_lookup_route);

router.get('/api/v1/accounts/:id', get_account_by_id_route);
router.post('/api/v1/accounts/:id/pin', endorse_account_route);
router.post('/api/v1/accounts/:id/mute', mute_account_route);
router.post('/api/v1/accounts/:id/note', notImplementedResponse);
router.post('/api/v1/accounts/:id/unpin', notImplementedResponse);
router.post('/api/v1/accounts/:id/remove_from_followers', notImplementedResponse);
router.post('/api/v1/accounts/:id/block', block_account_route);
router.post('/api/v1/accounts/:id/unblock', notImplementedResponse);
router.post('/api/v1/accounts/:id/unmute', notImplementedResponse);
router.post('/api/v1/accounts/:id/unfollow', notImplementedResponse);
router.post('/api/v1/accounts/:id/follow', notImplementedResponse);
router.get('/api/v1/accounts/:id', get_account_by_id_route);
router.get('/api/v1/accounts/:id/identity_proofs',notImplementedResponse);
router.get('/api/v1/accounts/:id/lists',notImplementedResponse);
router.get('/api/v1/accounts/:id/following',notImplementedResponse);
router.get('/api/v1/accounts/:id/statuses',get_statuses_by_account_id_route);

router.post('/api/v1/polls/:id/votes', cast_vote_route);
router.get('/api/v1/polls/:id', get_poll_route);

router.post('/api/pleroma/follow_import',notImplementedResponse);
router.post('/api/pleroma/mutes_import',notImplementedResponse);
router.post('/api/pleroma/blocks_import',notImplementedResponse);
router.get('/api/v1/pleroma/birthdays',notImplementedResponse);
router.post('/api/pleroma/disable_account',notImplementedResponse);
router.post('/api/pleroma/delete_account',notImplementedResponse);
router.post('/api/pleroma/change_password',notImplementedResponse);
router.post('/api/v1/pleroma/accounts/confirmation_resend',notImplementedResponse);
router.post('/api/pleroma/change_email', notImplementedResponse);
router.post('/api/pleroma/move_account',notImplementedResponse);
router.delete('/api/pleroma/aliases',notImplementedResponse);
router.get('/api/pleroma/aliases',notImplementedResponse);
router.put('/api/pleroma/aliases',notImplementedResponse);
router.post('/api/v1/pleroma/backups',notImplementedResponse);
router.get('/api/v1/pleroma/accounts/:id/favourites',notImplementedResponse);
router.post('/api/v1/pleroma/accounts/:id/unsubscribe', notImplementedResponse);
router.post('/api/v1/pleroma/accounts/:id/subscribe', notImplementedResponse);

router.get('/api/v1/mutes',notImplementedResponse);
router.get('/api/v1/blocks',notImplementedResponse);

router.delete('/api/v1/domain_blocks',notImplementedResponse);
router.get('/api/v1/domain_blocks',notImplementedResponse);
router.post('/api/v1/domain_blocks',notImplementedResponse);

router.get('/api/v1/filters',notImplementedResponse);
router.post('/api/v1/filters',notImplementedResponse);
router.delete('/api/v1/filters/:id',notImplementedResponse);
router.post('/api/v1/filters/:id',notImplementedResponse);
router.put('/api/v1/filters/:id',notImplementedResponse);

router.post('/api/v1/follow_requests/:id/reject',notImplementedResponse);
router.get('/api/v1/follow_requests',notImplementedResponse);
router.post('/api/v1/follow_requests/:id/authorize',notImplementedResponse);

router.delete('/api/v1/conversations/:id',notImplementedResponse);
router.post('/api/v1/pleroma/conversations/read',notImplementedResponse);
router.get('/api/v1/conversations',notImplementedResponse);
router.post('/api/v1/conversations/:id/read',notImplementedResponse);
router.get('/api/v1/pleroma/conversations/:id/statuses',notImplementedResponse);
router.get('/api/v1/pleroma/conversations/:id',notImplementedResponse);
router.patch('/api/v1/pleroma/conversations/:id',notImplementedResponse);

router.delete('/api/v1/pleroma/statuses/:id/reactions/:emoji',notImplementedResponse);
router.get('/api/v1/pleroma/statuses/:id/reactions/:emoji',notImplementedResponse);
router.put('/api/v1/pleroma/statuses/:id/reactions/:emoji',notImplementedResponse);
router.put('/api/v1/pleroma/statuses/:id/reactions',notImplementedResponse);

router.get('/api/v1/pleroma/emoji',notImplementedResponse);
router.get('/api/v1/custom_emojis',notImplementedResponse);

router.get('/api/v1/instance',notImplementedResponse);
router.get('/api/v1/instance/peers',notImplementedResponse);
router.get('/api/v1/pleroma/federation_status',notImplementedResponse);

router.get('/api/v1/pleroma/mascot',notImplementedResponse);
router.post('/api/v1/pleroma/mascot',notImplementedResponse);

router.get('/api/v1/accounts/:id/followers',notImplementedResponse);
router.get('/api/v1/pleroma/accounts/:id/endorsements',notImplementedResponse);
router.get('/api/v1/endorsements',notImplementedResponse);

router.get('/api/v1/statuses/:id/context', status_context_route);
router.get('/api/v1/statuses/:id', get_status_route)
router.post('/api/v1/statuses/:id/bookmark', bookmark_status_route);
router.post('/api/v1/statuses/:id/favourite', favourite_status_route);
router.post('/api/v1/statuses', create_status_route);
router.delete('/api/v1/statuses/:id', delete_status_route);

router.get('/manifest.json', manifest_route);

router.get('/api/v1/notifications/:id',notImplementedResponse);
router.post('/api/v1/notifications/dismiss',notImplementedResponse);
router.post('/api/v1/notifications/clear',notImplementedResponse);
router.get('/api/v1/notifications',notImplementedResponse);
router.post('/api/v1/pleroma/notifications/read',notImplementedResponse);
router.post('/api/v1/notifications/:id/dismiss',notImplementedResponse);
router.delete('/api/v1/notifications/destroy_multiple',notImplementedResponse);

router.get('/api/v1/pleroma/healthcheck',notImplementedResponse);
router.get('/api/v1/directory',notImplementedResponse);
router.get('/api/v1/pleroma/captcha',notImplementedResponse);
router.get('/api/pleroma/frontend_configurations',notImplementedResponse);

router.delete('/api/v1/push/subscription',notImplementedResponse);
router.get('/api/v1/push/subscription',notImplementedResponse);
router.post('/api/v1/push/subscription',notImplementedResponse);
router.put('/api/v1/push/subscription',notImplementedResponse);

router.post('/api/v1/pleroma/remote_interaction',notImplementedResponse);
router.get('/main/ostatus',notImplementedResponse);
router.post('/main/ostatus',notImplementedResponse);

router.get('/api/v0/pleroma/reports/:id', notImplementedResponse);
router.get('/api/v0/pleroma/reports', notImplementedResponse);
router.post('/api/v1/reports', notImplementedResponse);

router.delete('/api/v1/scheduled_statuses/:id', notImplementedResponse);
router.get('/api/v1/scheduled_statuses/:id', notImplementedResponse);
router.put('/api/v1/scheduled_statuses/:id', notImplementedResponse);
router.get('/api/v1/scheduled_statuses', notImplementedResponse);

router.get('/api/v1/search', notImplementedResponse);
router.get('/api/v2/search', notImplementedResponse);

router.get('/api/v1/pleroma/settings/{app}', notImplementedResponse);
router.patch('/api/v1/pleroma/settings/{app}', notImplementedResponse);

router.delete('/api/v1/lists/:id/accounts', notImplementedResponse);
router.get('/api/v1/lists/:id/accounts', notImplementedResponse);
router.post('/api/v1/lists/:id/accounts', notImplementedResponse);
router.get('/api/v1/lists', notImplementedResponse);
router.post('/api/v1/lists', notImplementedResponse);
router.delete('/api/v1/lists/:id', notImplementedResponse);
router.delete('/api/v1/lists/:id', notImplementedResponse);
router.put('/api/v1/lists/:id', notImplementedResponse);

router.get('/api/v1/bookmarks', notImplementedResponse);
router.get('/api/v1/favourites', notImplementedResponse);

router.delete('/api/v1/suggestions/:id', notImplementedResponse);
router.get('/api/v2/suggestions', notImplementedResponse);

router.get('/api/v1/timelines/home', home_timeline_route);
router.get('/api/v1/timelines/public', public_timeline_route);
router.get('/api/v1/timelines/direct', notImplementedResponse);
router.get('/api/v1/timelines/tag/:tag', notImplementedResponse);
router.get('/api/v1/timelines/list/:list_id', notImplementedResponse);

router.get('/api/v1/trends/tags', notImplementedResponse);
router.get('/api/v1/trends', notImplementedResponse);



export default {
	fetch: router.fetch, // Assuming you're using Hono in your main router as well
};
