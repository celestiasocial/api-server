import { load } from 'https://deno.land/std@0.196.0/dotenv/mod.ts';
import { Hono, HonoRequest } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import apirouter from './apirouter.ts';
import jwt from 'https://esm.sh/jsonwebtoken@9.0.1';
import { findActorByUsername } from './functions/findActorByUsername.ts';

const env = await load();
const router = new Hono();

function getBearerToken(context: HonoRequest) {
	const authHeader = context.header('Authorization');
	if (authHeader && authHeader.startsWith('Bearer ')) {
		return authHeader.slice(7); // Remove 'Bearer ' to get the token part
	}
	return null;
}

// This let's us get the payload from other routes :)
export const getPayloadFromContext = (context: HonoRequest) => {
	const auth = context.header('Authorization') || '';
	return jwt.decode(auth?.slice(7));
}

/**
 *
 * @param context Pass context.req to this function
 *
 * @return Actor Actor (the Actor object of the current user)
 */
export const getActorFromContext = async (context: HonoRequest) => {
	const auth = jwt.decode(context.header('Authorization')?.slice(7))
	const username = auth.user.username;
	const actor = await findActorByUsername(username);
	return actor.properties;
}

const corsHeaders = {
	'Access-Control-Allow-Origin': '*',
	'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE, OPTIONS',
	'Access-Control-Allow-Headers': 'Content-Type, Authorization',
	'Access-Control-Max-Age': '86400', // 1 day (in seconds)
};

// Handle preflight requests
router.options('*', () => new Response(null, { headers: corsHeaders }));

// Handle all other requests
const skipUrls = [
	/^\/api\/pleroma\/captcha$/,
	/^\/api\/v1\/accounts$/,
	/^\/api\/v1\/accounts\/lookup$/,
	/^\/api\/v1\/apps$/,
	/^\/api\/v1\/instance$/,
	/^\/api\/pleroma\/frontend_configurations$/,
];

router.use('*', async (c, next) => {
	const pathname = c.req.path;

	// If the request URL is in the skipUrls array, don't check the JWT
	if (pathname.startsWith('/api') && !skipUrls.some((pattern) => pattern.test(pathname))) {
		if (c.req.method === 'OPTIONS') {
			// preflight request. reply successfully:
			return new Response(null, { status: 204 });
		} else {
			// @ts-ignore
			const token = getBearerToken(c.req);
			if (!token) {
				return new Response('Unauthorized', { status: 401 });
			}
			try {
				// Verifying token
				// @ts-ignore
				const isValid = await jwt.verify(token, Deno.env.get("JWT_SECRET"));
				if (!isValid) {
					return new Response('Invalid token', { status: 401 });
				}
				// Attach the payload
			} catch (error) {
				console.log(error)
				return new Response('Invalid token', { status: 401 });
			}
		}
	}

	// @ts-ignore: Reverse proxy with a number of different data formats.
	const response = await apirouter.fetch(c.req);

	if (response.status == 404 && !pathname.startsWith('/api')) {
		// Return a default response if none was returned by the apirouter.
		const pathname  = c.req.path;
		try {
			return await fetch('https://soapbox-celestiasocial-tcnho.ondigitalocean.app' + pathname)
				.then(response => {
					const clone = response.clone();
					return clone.blob().then(data => {
						return new Response(data, {
							status: response.status,
							statusText: response.statusText,
							headers: response.headers
						});
					});
				})
		} catch (e: any) {
			return new Response(e.message, { status: 500 });
		}
	} else {
		// Add CORS headers to the response
		const newHeaders = new Headers(response.headers);
		Object.keys(corsHeaders).forEach((name) => {
			// @ts-ignore
			newHeaders.set(name, corsHeaders[name]);
		});

		return new Response(response.body, {
			status: response.status,
			statusText: response.statusText,
			headers: newHeaders,
		});
	}
});

export default {
	fetch: router.fetch, // Assuming you're using Hono in your main router as well
};
