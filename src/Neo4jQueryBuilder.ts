import { load } from "https://deno.land/std@0.196.0/dotenv/mod.ts";
import neo4j, { Driver } from "https://deno.land/x/neo4j_lite_client@4.4.1-preview2/mod.ts";
const env = await load();

/**
 * Singleton class to manage Neo4j query building.
 */
export class Neo4jQueryBuilder {
  private static instance: Neo4jQueryBuilder;
  private driver: Driver;

  private constructor() {
    this.driver = neo4j.driver(
      env["NEO4J_URL"],
      neo4j.auth.basic(env["NEO4J_USER"], env["NEO4J_PASS"]),
      {
        maxConnectionLifetime: 30 * 60 * 1000,
        connectionAcquisitionTimeout: 2 * 60 * 1000,
      }
    );
  }

  /**
   * Returns an instance of Neo4jQueryBuilder.
   * @returns {Neo4jQueryBuilder} The singleton instance.
   */
  public static getInstance(): Neo4jQueryBuilder {
    if (!this.instance) {
      this.instance = new Neo4jQueryBuilder();
    }

    return this.instance;
  }

  /**
   * Starts a new query building context.
   * @returns {QueryBuilderContext} The context for building a query.
   */
  public query(): QueryBuilderContext {
    return new QueryBuilderContext(this.driver);
  }
}

/**
 * Class to manage the state of multiple queries without the risk of interfering with
 * each other while still sharing the same Neo4j Driver object.
 */
class QueryBuilderContext {
  private driver: Driver;
  private query: string[];
  private params: Record<string, any>;

  /**
   * Constructs a new QueryBuilderContext.
   * @param {Driver} driver - The Neo4j driver.
  */
  public constructor(driver: Driver) {
    this.driver = driver;
    this.query = [];
    this.params = {};
  }

  // Internal method to reset the query
  private resetQuery(): void {
    this.query = [];
    this.params = {};
  }

  /**
 * Adds a MATCH clause to the query.
 * @param {string} identifier - The node identifier.
 * @param {string} [label] - Optional node label.
 * @returns {this} The current query context.
 */
  public match(identifier: string): this;
  public match(identifier: string, label: string): this;
  public match(identifier: string, properties: Record<string, any>): this;
  public match(identifier: string, labelOrProperties?: string | Record<string, any>, properties?: Record<string, any>): this {
    let labelString = '';
    let propertiesString = '';
  
    if (typeof labelOrProperties === 'string') {
      labelString = `:${labelOrProperties}`;
    } else if (typeof labelOrProperties === 'object') {
      properties = labelOrProperties;
    }
  
    if (properties) {
      propertiesString = ` {${Object.entries(properties).map(([key, value]) => `${key}: $${key}`).join(', ')}}`;
      this.params = { ...this.params, ...properties };
    }
  
    this.query.push(`MATCH (${identifier}${labelString}${propertiesString})`);
    return this;
  }  

  public matchWithRelationship(pattern: string): this {
    this.query.push(`MATCH ${pattern}`);
    return this;
  }  

  /**
 * Adds a CREATE clause to the query.
 * @param {string} identifier - The node identifier.
 * @param {string} [label] - Optional node label.
 * @param {Record<string, any>} [fields] - Optional fields to create.
 * @returns {this} The current query context.
 */
  public create(identifier: string, label?: string, fields: Record<string, any> = {}): this {
    const fieldString = Object.entries(fields)
      .map(([key, value]) => `${key}: ${typeof value === 'string' ? '"' + value + '"' : value}`)
      .join(', ');
    if (label) {
      this.query.push(`CREATE (${identifier}:${label} {${fieldString}})`);
    } else {
      this.query.push(`CREATE ${identifier}`);
    }
    return this;
  }

  /**
 * Adds a MERGE clause to the query.
 * @param {string} identifier - The node identifier.
 * @param {string} [label] - Optional node label.
 * @param {Record<string, any>} [fields] - Optional fields to merge.
 * @returns {this} The current query context.
 */
  public merge(identifier: string, labelOrProperties?: string | Record<string, any>, labelIfProperties?: string): this {
    let propertiesString = '';
    let label = '';

    if (typeof labelOrProperties === 'object') {
      propertiesString = ` {${Object.entries(labelOrProperties).map(([key, value]) => `${key}: $${key}`).join(', ')}}`;
      this.params = { ...this.params, ...labelOrProperties };
      if (labelIfProperties) {
        label = `:${labelIfProperties}`;
      }
    } else if (typeof labelOrProperties === 'string') {
      label = `:${labelOrProperties}`;
    }

    this.query.push(`MERGE (${identifier}${label}${propertiesString})`);

    return this;
  }


  /**
   * Adds a SET clause to the query.
   * @param {string} identifier - The node identifier.
   * @param {Record<string, any>} fields - The fields to set.
   * @returns {this} The current query context.
   */
  public set(identifier: string, fields: Record<string, any>): this {
    const assignments = Object.entries(fields).map(([key, value]) => `${identifier}.${key} = $${key}`);
    this.query.push(`SET ${assignments.join(', ')}`);
    this.params = { ...this.params, ...fields };
    return this;
  }

  /**
 * Adds a REMOVE clause to the query to unset properties from a node.
 * @param {string} identifier - The node identifier.
 * @param {string[]} fieldNames - The field names to remove.
 * @returns {this} The current query context.
 */
  public unset(identifier: string, fieldNames: string[]): this {
    this.query.push(`REMOVE ${fieldNames.map(fieldName => `${identifier}.${fieldName}`).join(', ')}`);
    return this;
  }

  /**
 * Updates a node by setting the specified fields.
 * @param {string} node - The node identifier.
 * @param {Record<string, any>} fields - The fields to update.
 * @returns {this} The current query context.
 */
  public update(node: string, fields: Record<string, any>): this {
    return this.set(node, fields);
  }

  /**
 * Deletes nodes and/or relationships from the query.
 * @param {string[]} [nodes] - Optional nodes to delete.
 * @param {string[]} [relationships] - Optional relationships to delete.
 * @returns {this} The current query context.
 */
  public delete(nodes?: string[], relationships?: string[]): this {
    if (nodes && nodes.length > 0) {
      this.query.push(`DELETE ${nodes.join(', ')}`);
    }
    if (relationships && relationships.length > 0) {
      this.query.push(`DELETE ${relationships.join(', ')}`);
    }
    return this;
  }

  /**
 * Deletes a specific relationship between nodes.
 * @param {string} fromNode - The starting node identifier.
 * @param {string} fromLabel - The starting node label.
 * @param {string} relationshipType - The type of the relationship.
 * @param {string} toNode - The ending node identifier.
 * @param {string} toLabel - The ending node label.
 * @returns {this} The current query context.
 */
  public deleteRelationship(fromNode: string, fromLabel: string, relationshipType: string, toNode: string, toLabel: string): this {
    this.query.push(`MATCH (${fromNode}:${fromLabel})-[r:${relationshipType}]->(${toNode}:${toLabel}) DELETE r`);
    return this;
  }

  /**
 * Adds a WITH clause to the query.
 * @param {string | string[]} expressions - Expressions for the WITH clause.
 * @returns {this} The current query context.
 */
  public with(expressions: string | string[]): this {
    const expressionString = Array.isArray(expressions) ? expressions.join(', ') : expressions;
    this.query.push(`WITH ${expressionString}`);
    return this;
  }

  /**
 * Adds a WHERE clause to the query.
 * @param {string} condition - The condition for the WHERE clause.
 * @param {Record<string, any>} parameters - Parameters for the condition.
 * @returns {this} The current query context.
 */
  public where(condition: string, parameters: Record<string, any>): this {
    this.query.push(`WHERE ${condition}`);
    this.params = { ...this.params, ...parameters };
    return this;
  }

  /**
 * Adds an ORDER BY clause to the query.
 * @param {string[]} fields - The fields to order by.
 * @param {string} [direction='ASC'] - Optional direction of sorting ('ASC' or 'DESC').
 * @returns {this} The current query context.
 */
  public orderBy(fields: string[], direction: string = 'ASC'): this {
    this.query.push(`ORDER BY ${fields.join(', ')} ${direction}`);
    return this;
  }

  /**
 * Adds a RETURN clause to the query.
 * @param {string[]} fields - The fields to return.
 * @returns {this} The current query context.
 */
  public return(fields: string[]): this {
    this.query.push(`RETURN ${fields.join(', ')}`);
    return this;
  }

  /**
 * Adds a LIMIT clause to the query.
 * @param {number} value - The number of records to limit.
 * @returns {this} The current query context.
 */
  public limit(value: number): this {
    this.query.push(`LIMIT toInteger(${value})`);
    return this;
  }

  /**
 * Adds a SKIP clause to the query.
 * @param {number} value - The number of records to skip.
 * @returns {this} The current query context.
 */
  public skip(value: number): this {
    this.query.push(`SKIP toInteger(${value})`);
    return this;
  }

  /**
   * Inserts custom Cypher query text into the query.
   * @param {string} customQuery - The custom Cypher query text.
   * @returns {this} The current query context.
   */
  public custom(customQuery: string): this {
    this.query.push(customQuery);
    return this;
  }

  /**
 * Validates the constructed query for common syntax patterns and errors.
 * @private
 * @returns {boolean} True if the query is valid, false otherwise.
 */
  private validateQuery(): boolean {
    if (this.query.length === 0) {
      return false; // Query must not be empty
    }

    const fullQuery = this.query.join(' ');

    // Query must start with a known keyword (e.g., MATCH, CREATE, MERGE)
    if (!/^\s*(MATCH|CREATE|MERGE)/i.test(fullQuery)) {
      return false;
    }

    // Check for balanced parentheses
    if ((fullQuery.match(/\(/g) || []).length !== (fullQuery.match(/\)/g) || []).length) {
      return false;
    }

    // Check for common syntax patterns, like a valid RETURN statement
    if (fullQuery.includes('RETURN') && !/\bRETURN\s+\w/i.test(fullQuery)) {
      return false;
    }

    // Check for common syntax patterns, like a valid WHERE statement
    if (fullQuery.includes('WHERE') && !/\bWHERE\s+\w+/i.test(fullQuery)) {
      return false;
    }

    // Check for valid ORDER BY statement
    if (fullQuery.includes('ORDER BY') && !/\bORDER BY\s+\w+\s+(ASC|DESC)?/i.test(fullQuery)) {
      return false;
    }

    // Check for valid LIMIT statement
    if (fullQuery.includes('LIMIT') && !/\bLIMIT\s+toInteger\(\d+\)/i.test(fullQuery)) {
      return false;
    }

    // Check for valid SKIP statement
    if (fullQuery.includes('SKIP') && !/\bSKIP\s+toInteger\(\d+\)/i.test(fullQuery)) {
      return false;
    }

    // Ensure that DELETE doesn't appear without a preceding MATCH
    if (fullQuery.includes('DELETE') && !fullQuery.includes('MATCH')) {
      return false;
    }

    // Check for unclosed strings
    if ((fullQuery.match(/"/g) || []).length % 2 !== 0) {
      return false;
    }

    return true; // Query is valid
  }

  /**
   * Executes the query.
   * @param {Record<string, any>} [params] - Optional parameters for the query.
   * @returns {Promise<any[]>} The result of the query.
   * @throws {Error} If the Cypher query is invalid.
   */
  public async execute(params?: Record<string, any>): Promise<any[]> {
    if (params) {
      this.params = { ...this.params, ...params }; // Merge with existing parameters
    }

    // This is a soft validation which logs the query with a method that it's invalid
    // It will still attempt to execute the query.
    if (!this.validateQuery()) {
      console.log(`Invalid Query: `, this.query, this.params)
    }

    const session = this.driver.session();
    const txc = session.beginTransaction();
    const fullQuery = this.query.join(' ');

    // Integrated Query Logger
    // This logs every query, params, and the Stack Trace
    console.log(`Query:`, fullQuery);
    console.log(`Params:`, this.params);
    const stackTrace = new Error('Neo4jQueryBuilderStackTraceLogger').stack;
    console.log("Stack Trace:", stackTrace);

    try {
      const result = await txc.run(fullQuery, this.params);
      await txc.commit();
      this.resetQuery();
      return result.records;
    } catch (error) {
      await txc.rollback();
      throw error;
    } finally {
      // Reset the query after execution
      this.query = [];
      this.params = {};
      try {
        await session.close();
      } catch (err) {
        console.error("Failed to close session:", err);
      }
    }
  }

  /**
* Executes the query with specific parameters.
* @param {Record<string, any>} params - Parameters for the query.
* @returns {Promise<any[]>} The result of the query.
*/
  public async executeWithParams(params: Record<string, any>): Promise<any[]> {
    return this.execute(params); // Simply call the execute method with parameters
  }

  public close(): void { } // Keep the same driver instance as this is a Singleton class.
}