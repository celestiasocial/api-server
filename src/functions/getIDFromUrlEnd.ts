export const getIDFromUrlEnd = (url: string) => {
	try {
		const splitUrl = url.split('/');
		return splitUrl.length > 1 ? splitUrl[splitUrl.length - 1] : url;
	} catch (error) {
		console.error('Error:', error);
		return '';
	}
};
