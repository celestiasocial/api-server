export const convertNeo4jDateTimeToJSTS = (neo4jDateTime: any): string => {
	const date = new Date(Date.UTC(
		neo4jDateTime.year.low,
		neo4jDateTime.month.low - 1,
		neo4jDateTime.day.low,
		neo4jDateTime.hour.low,
		neo4jDateTime.minute.low,
		neo4jDateTime.second.low,
		Math.floor(neo4jDateTime.nanosecond.low / 1000000)
	));

	return date.toISOString();
};
