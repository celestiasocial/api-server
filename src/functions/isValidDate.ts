// Helper function to validate date format (YYYY-MM-DD)
const isValidDate = (date: string): boolean => /^\d{4}-\d{2}-\d{2}$/.test(date);
export default isValidDate;
