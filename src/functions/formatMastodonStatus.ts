import { Neo4jQueryBuilder } from '../Neo4jQueryBuilder.ts';
import { getIDFromUrlEnd } from './getIDFromUrlEnd.ts';
import { transformActorToAccount } from './transformActorToAccount.ts';
import { transformPollToMastodonPoll } from './transformPollToMastodonPoll.ts';
import { findOrCreateActor } from './findOrCreateActor.ts';
import { transformMediaToMastodonMedia } from "./transformMediaToMastodonMedia.ts";
import { isStatusFavourited } from "./isStatusFavourited.ts";
import { isStatusBookmarked } from "./isStatusBookmarked.ts";

const getActorUsername = (noteProps: any) => {
	if (noteProps.attributedTo) {
		return noteProps.attributedTo.split('/').pop();
	} else {
		throw new Error('Note has no attributedTo property');
	}
}

const getMediaAttachments = async (noteId: string) => {
	const queryBuilder = Neo4jQueryBuilder.getInstance().query();
	const result = await queryBuilder
		.match('n', 'Note')
		.where('n.id = $noteId', { noteId })
		.match('(n)-[:HAS_MEDIA]->(m:Media)')
		.return(['m'])
		.execute();

	return result.map((record: any) => {
		const media = record.get('m');
		return transformMediaToMastodonMedia(media.properties);
	});
}

const getPoll = async (noteId: string, currentUser: string) => {
	const queryBuilder = Neo4jQueryBuilder.getInstance().query();
	const pollResult = await queryBuilder
		.match('n', 'Note')
		.where('n.id = $noteId', { noteId })
		.match('(n)-[:HAS_POLL]->(p:Poll)')
		.return(['p'])
		.execute();

	if (pollResult.length > 0) {
		return await transformPollToMastodonPoll(pollResult[0].get('p').properties, currentUser);
	}
	return null;
}

export const formatMastodonStatus = async (note: any, currentUsername: string): Promise<any> => {
	const noteProps = note.properties ? note.properties : note;

	if (!noteProps.objectId) {
		throw new Error('Status Missing Object ID');
	}

	const actorUsername = getActorUsername(noteProps);
	const actor = await findOrCreateActor(actorUsername);

	const media_attachments = await getMediaAttachments(noteProps.id);
	const poll = await getPoll(noteProps.id, currentUsername);

	const mastodonStatus = {
		id: noteProps.objectId || getIDFromUrlEnd(noteProps.id),
		uri: noteProps.id,
		created_at: noteProps.published,
		account: transformActorToAccount(actor),
		content: noteProps.content,
		visibility: 'public',
		sensitive: noteProps.sensitive,
		spoiler_text: noteProps.summary,
		media_attachments,
		poll,
		application: { name: 'Web', website: null },
		mentions: [],
		tags: [],
		emojis: [],
		reblogs_count: 0,
		favourites_count: 0,
		replies_count: 0,
		url: noteProps.id,
		in_reply_to_id: noteProps.inReplyTo ? noteProps.inReplyTo : undefined,
		in_reply_to_account_id: noteProps.inReplyToAuthor ? noteProps.inReplyToAuthor : undefined,
		favourited: await isStatusFavourited({ username: currentUsername, statusId: noteProps.objectId }),
		bookmarked: await isStatusBookmarked({ username: currentUsername, statusId: noteProps.objectId }),
	};

	return mastodonStatus;
};