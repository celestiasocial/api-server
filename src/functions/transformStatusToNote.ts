import { Note } from '../schemas/activitypub.ts';
import uuid62 from 'npm:uuid62';

export const transformStatusToNote = async (status: any, username: string): Promise<Note> => {
	const webScheme = Deno.env.get('WEB_SCHEME');
	const webDomain = Deno.env.get('WEB_DOMAIN');
	const uuid = await uuid62.v4();
	const actorId = `${webScheme}://${webDomain}/${username}`;

	return {
		type: 'Note',
		id: `${actorId}/${uuid}`,
		to: [`${actorId}/followers`],
		cc: [`${webScheme}://${webDomain}/public`],
		content: status.status,
		attributedTo: actorId,
		published: new Date().toISOString(),
		sensitive: status.sensitive,
		summary: status.spoiler_text,
		inReplyTo: status.in_reply_to_id ? status.in_reply_to_id : undefined,
		media_ids: status.media_ids,
	};
};
