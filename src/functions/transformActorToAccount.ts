type ActorProperties = {
	objectId?: string;
	preferredUsername?: string;
	id?: string;
	display_name?: string;
	note?: string;
	createdAt?: string;
	[key: string]: any;
};

type Actor = {
	properties?: ActorProperties;
	[key: string]: any;
};

export const transformActorToAccount = (actor: Actor): any => {
	const getActorProperty = (property: string): any => actor.properties?.[property] ?? actor[property]; // Helper function to get actor property

	const account = {
		id: getActorProperty('objectId'),
		username: getActorProperty('preferredUsername'),
		acct: getActorProperty('preferredUsername'),
		url: getActorProperty('id'),
		display_name: getActorProperty('display_name'),
		note: getActorProperty('note'),
		avatar: '', // Set the URL of the avatar
		avatar_static: '', // Set the URL of the static avatar
		header: '', // Set the URL of the header image
		header_static: '', // Set the URL of the static header image
		locked: false, // Set to true if the account is locked
		created_at: getActorProperty('createdAt'), // Assuming actor has createdAt property
		followers_count: 0, // Set the follower count
		following_count: 0, // Set the following count
		statuses_count: 0, // Set the statuses count
		fqn: `${getActorProperty('preferredUsername')}@${Deno.env.get('WEB_DOMAIN')}`,
	};
	return account;
};
