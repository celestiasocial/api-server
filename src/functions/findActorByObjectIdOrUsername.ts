import { Neo4jQueryBuilder } from "../Neo4jQueryBuilder.ts";

export const findActorByUsernameOrId = async (identifier: string) => {
    if (!identifier) {
        throw new Error("Identifier cannot be null or undefined.");
    }

    try {
        // Get the Singleton instance of Neo4jQueryBuilder
        const queryBuilder = Neo4jQueryBuilder.getInstance().query();

        // Construct the query using the query builder
        const result = await queryBuilder
            .match('a', 'Actor')
            .where('a.id = $identifier OR a.objectId = $identifier OR a.preferredUsername = $identifier', { identifier })
            .return(['a'])
            .execute();

        if (result.length === 0) {
            throw new Error(`No actor found with identifier: ${identifier}`);
        }

        return result[0].get('a');

    } catch (error: any) {
        throw new Error(`Failed to get actor by identifier due to error: ${error}`);
    }
}