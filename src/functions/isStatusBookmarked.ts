import { Neo4jQueryBuilder } from '../Neo4jQueryBuilder.ts';

interface IBookmarkedStatus {
	username: string;
	statusId: string;
}

export const isStatusBookmarked = async ({username, statusId}: IBookmarkedStatus): Promise<boolean | undefined> => {
	if (!username || !statusId) {
		throw new Error('Username and statusId must be defined');
	}

	const queryBuilder = Neo4jQueryBuilder.getInstance().query();
	let isBookmarked;

	try {
		const result = await queryBuilder
			.match('a', 'Actor')
			.where('a.preferredUsername = $username', { username })
			.match('(a)-[r:BOOKMARKED]->(n:Note)')
			.where('n.objectId ENDS WITH $statusId', { statusId })
			.return(['r'])
			.execute();

		isBookmarked = result.length > 0;
	} catch (error) {
		console.error(`Failed to check if status is bookmarked: ${error}`);
	}

	return isBookmarked;
};
