import { Neo4jQueryBuilder } from "../Neo4jQueryBuilder.ts";

export const findActorByUsername = async (username: string) => {
    try {
        // Get the Singleton instance of Neo4jQueryBuilder
        const queryBuilder = Neo4jQueryBuilder.getInstance().query();

        // Construct the query using the query builder
        const result = await queryBuilder
            .match('a', 'Actor')
            .where('a.preferredUsername = $username', { username })
            .return(['a'])
            .execute();

        if (result.length === 0) {
            return null;
        }

        const record = result[0];
        return record.get('a');
    } catch (error) {
        console.error(`Error occurred while finding actor by username: ${username}`, error);
        throw error;
    }
};
