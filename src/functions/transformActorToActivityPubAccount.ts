import neo4j from '../neo4j.ts';

export const transformActorToActivityPubAccount = (username: string, domain: string) => {
	const urlBase = `https://${domain}/users/${username}`;
	let account = {
		"@context": "https://www.w3.org/ns/activitystreams",
		"id": urlBase,
		"type": "Person",
		"following": `${urlBase}/following`,
		"followers": `${urlBase}/followers`,
		"inbox": `${urlBase}/inbox`,
		"outbox": `${urlBase}/outbox`,
		"preferredUsername": username,
		"endpoints": {
			"sharedInbox": `https://${domain}/inbox`
		},
		"url": `https://${domain}/@${username}`
	};

	return account;
};

const driver = neo4j;
