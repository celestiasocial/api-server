import { Neo4jQueryBuilder } from '../Neo4jQueryBuilder.ts';
import { formatMastodonStatus } from './formatMastodonStatus.ts';

export const getNotesByUsername = async (username: string) => {
	const queryBuilder = Neo4jQueryBuilder.getInstance().query();

	try {
		const result = await queryBuilder
			.match('a', 'Actor')
			.where('a.preferredUsername = $username', { username })
			.match('(a)-[:CREATED]->(n:Note)')
			.return(['a', 'n'])
			.orderBy(['n.published'], 'DESC')
			.limit(100)
			.execute();

		const notes = result.map((record: any) => {
			const note = record.get('n');
			const actor = record.get('a');
			note.account = actor;
			return formatMastodonStatus(note, actor.properties.preferredUsername);
		});

		// Await all promises to resolve
		const formattedNotes = await Promise.all(notes);

		return formattedNotes;
	} catch (error: any) {
		// Improved error handling: Throw an error instead of just logging it.
		// This allows the caller to decide what to do with the exception.
		throw new Error(`Failed to get notes by username due to error: ${error}`);
	}
};
