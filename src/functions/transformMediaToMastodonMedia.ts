import { getIDFromUrlEnd } from './getIDFromUrlEnd.ts';

export const transformMediaToMastodonMedia = (media: any): any => {
	return {
		id: getIDFromUrlEnd(media.id),  // Assuming media id is in URL format
		type: 'image',  // Update this as per your media type. In Mastodon API, it could be: 'image', 'video', 'gifv', 'audio'
		url: media.url,
		preview_url: media.preview_url,
		remote_url: media.remote_url,
		text_url: media.text_url,
		description: media.description,
		meta: null,  // Update this as per your requirements.
	};
};
