import { Neo4jQueryBuilder } from '../Neo4jQueryBuilder.ts';

export const getPollById = async (pollId: string) => {
	const queryBuilder = Neo4jQueryBuilder.getInstance().query();
	
	const result = await queryBuilder
		.match('p', 'Poll')
		.where('p.objectId = $pollId', { pollId })
		.return(['p'])
		.execute();

	if (result.length === 0) {
		return null;
	}

	const record = result[0];
	const poll = record.get('p');

	return poll.properties;
};
