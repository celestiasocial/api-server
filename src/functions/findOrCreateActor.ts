import { Neo4jQueryBuilder } from "../Neo4jQueryBuilder.ts";
import uuid62 from 'npm:uuid62';

export const findOrCreateActor = async (username: string) => {
    // Construct the actorId and inbox
    const actorId = `${Deno.env.get('WEB_SCHEME')}://${Deno.env.get('WEB_DOMAIN')}/${username}`;
    const inbox = `${actorId}/inbox`;

    // Get the Singleton instance of Neo4jQueryBuilder
    const queryBuilder = Neo4jQueryBuilder.getInstance().query();

    try {
        // Find existing actor by actorId
		const actor = await queryBuilder
            .match('a', 'Actor')
            .where('a.id = $actorId', { actorId })
            .return(['a'])
            .execute();

        if (actor && actor.length > 0) {
            return actor[0].get('a');
        }

        // If no existing actor, create a new one
        const objectId = uuid62.v4();
        const fields = {
            type: 'Person',
            objectId,
            preferredUsername: username,
            name: username,
            inbox,
            summary: ''
        };

        const createdActor = await queryBuilder
            .merge('a', 'Actor', { id: actorId })
            .set('a', fields)
            .return(['a'])
            .execute();

        if (createdActor && createdActor.length > 0) {
            return createdActor[0].get('a');
        }

    } catch (error) {
        console.error('An error occurred:', error);
        throw error;
    }

    return null;
};