import { Neo4jQueryBuilder } from '../Neo4jQueryBuilder.ts';

interface IFavouritedStatus {
	username: string;
	statusId: string;
}

export const isStatusFavourited = async ({username, statusId}: IFavouritedStatus): Promise<boolean | undefined> => {
	if (!username || !statusId) {
		throw new Error('Username and statusId must be defined');
	}

	const queryBuilder = Neo4jQueryBuilder.getInstance().query();
	let isFavourited;

	try {
		const result = await queryBuilder
			.match('a', 'Actor')
			.where('a.preferredUsername = $username', { username })
			.match('(a)-[r:FAVOURITED]->(n:Note)')
			.where('n.id ENDS WITH $statusId', { statusId })
			.return(['r'])
			.execute();

		isFavourited = result.length > 0;
	} catch (error) {
		throw new Error(`Failed to check if status is favourited: ${error}`);
	}

	return isFavourited;
};
