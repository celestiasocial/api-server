import { Neo4jQueryBuilder } from '../Neo4jQueryBuilder.ts';

export const getActorFromNote = async (note: any) => {
	const queryBuilder = Neo4jQueryBuilder.getInstance().query();

	const result = await queryBuilder
		.match('(a:Actor)-[:CREATED]->(n:Note)') // Define the pattern here
		.where('n.id = $noteId', { noteId: note.properties.id }) // Use WHERE clause to specify condition
		.return(['a'])
		.execute();

	if (result.length === 0) {
		throw new Error('Actor not found for this note');
	}

	const record = result[0];
	return record.get('a');
};
