import { Neo4jQueryBuilder } from "../Neo4jQueryBuilder.ts";

export const transformPollToMastodonPoll = async (poll: any, currentUser: string) => {
	const createdAt = new Date(poll.created_at);
	const expiresInMilliseconds = poll.expires_in * 1000; // Convert expiresIn to milliseconds
	const expired = (Date.now() - createdAt.getTime()) > expiresInMilliseconds;

	let totalVotesCount = await getTotalVotesCount(poll.objectId);
	let votersCount = await getUniqueVotersCount(poll.objectId);
	let currentUserHasVoted = await hasCurrentUserVoted(poll.objectId, currentUser);
	let currentUserVotes = currentUserHasVoted ? await getCurrentUserVotes(poll.objectId, currentUser) : [];

	const options = await Promise.all(poll.options.map(async (option: any, index: number) => {
		let votesCount = await getVotesCountForOption(poll.objectId, index);
		return {
			title: option,
			votes_count: votesCount,
		};
	}));

	const mastodonPoll = {
		id: poll.objectId,
		expires_at: new Date(createdAt.getTime() + expiresInMilliseconds).toISOString(),
		expired: expired,
		multiple: poll.multiple,
		votes_count: totalVotesCount,
		voters_count: votersCount,
		options: options,
		emojis: [], // Update this with actual data if necessary
		voted: currentUserHasVoted,
		own_votes: currentUserVotes,
	};

	return mastodonPoll;
};

const executeQuery = async (query, params) => {
	const queryBuilder = Neo4jQueryBuilder.getInstance().query();
	const result = await queryBuilder.custom(query).execute(params);
	if (result && result.length > 0) {
		return result[0];
	}
	return null; // Return null or appropriate default value if no records found
};

const getVotesCountForOption = async (pollId: string, choice: number) => {
	const query = 'MATCH (p:Poll {objectId: $pollId})<-[:IN_POLL]-(v:Vote {choice: $choice}) RETURN count(v) AS votesCount';
	return (await executeQuery(query, { pollId, choice })).get('votesCount').toNumber();
};

const getTotalVotesCount = async (pollId: string) => {
	const query = 'MATCH (p:Poll {objectId: $pollId})<-[:IN_POLL]-(v:Vote) RETURN count(v) AS totalVotesCount';
	return (await executeQuery(query, { pollId })).get('totalVotesCount').toNumber();
};

const getUniqueVotersCount = async (pollId: string) => {
	const query = 'MATCH (p:Poll {objectId: $pollId})<-[:IN_POLL]-(v:Vote)<-[:VOTED]-(a:Actor) RETURN count(DISTINCT a) AS votersCount';
	return (await executeQuery(query, { pollId })).get('votersCount').toNumber();
};

const hasCurrentUserVoted = async (pollId: string, currentUser: string) => {
	const query = 'MATCH (p:Poll {objectId: $pollId})<-[:IN_POLL]-(v:Vote)<-[:VOTED]-(a:Actor {preferredUsername: $currentUser}) RETURN count(v) > 0 AS hasVoted';
	return (await executeQuery(query, { pollId, currentUser })).get('hasVoted');
};

const getCurrentUserVotes = async (pollId: string, currentUser: string) => {
	const query = 'MATCH (p:Poll {objectId: $pollId})<-[:IN_POLL]-(v:Vote)<-[:VOTED]-(a:Actor {preferredUsername: $currentUser}) RETURN collect(v.choice) AS votes';
	const result = await executeQuery(query, { pollId, currentUser });
	return result.get('votes').map(vote => vote.toNumber());
};