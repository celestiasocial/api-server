import {z} from "https://deno.land/x/zod@v3.21.4/mod.ts";

export const votePayloadSchema = z.object({
	choices: z.array(z.string()).nonempty(),
});
