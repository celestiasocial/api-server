import { Hono, Context } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import neo4j from '../../neo4j.ts';
import { pemToPublicKey, verifyRequest } from 'https://gitlab.com/soapbox-pub/fedisign/-/raw/v0.2.1/mod.ts';
import { activitySchema } from "../../schemas/activitypub.ts";

async function parseAndValidateActivity(c: Context) {
	return activitySchema.parse(await c.req.json());
}

async function verifySignature(c: Context) {
	// @ts-ignore
	return verifyRequest(c.req, async (parsedSignature: any) => {
		const response = await fetch(parsedSignature.keyId, {
			headers: { Accept: 'application/activity+json' },
		});

		if (response.ok) {
			const actor = await response.json();
			return pemToPublicKey(actor.publicKey.publicKeyPem);
		} else {
			return null;
		}
	});
}

async function storeActor(activity: any, tx: any) {
	await tx.run(
		`MERGE (a:Actor {id: $id})`,
		{ id: activity.actor }
	);
}

async function handleCreateActivity(activity: any, tx: any) {
	if (activity.object.type === 'Note') {
		// Handle the creation of a status...
		// Here you would include the logic for creating a status in your database
	}
}

async function handleUpdateActivity(activity: any, tx: any) {
	if (activity.object.type === 'Note') {
		// Handle the update of a status...
		// Here you would include the logic for updating a status in your database
	}
}

async function handleDeleteActivity(activity: any, tx: any) {
	if (activity.object.type === 'Note') {
		// Handle the deletion of a status...
		// Here you would include the logic for deleting a status in your database
	}
}

async function handleAnnounceActivity(activity: any, tx: any) {
	console.log('Handle announce activity here', activity);
}

async function handleEmojiReactActivity(activity: any, tx: any) {
	console.log('Handle emoji react activity here', activity);
}

async function handleFollowActivity(activity: any, tx: any) {
	console.log('Handle follow activity here', activity);
}

async function handleLikeActivity(activity: any, tx: any) {
	console.log('Handle like activity here', activity);
}

async function handleActivity(activity: any, tx: any) {
	switch (activity.type) {
		case 'Create':
			await handleCreateActivity(activity, tx);
			break;
		case 'Update':
			await handleUpdateActivity(activity, tx);
			break;
		case 'Delete':
			await handleDeleteActivity(activity, tx);
			break;
		case 'Announce':
			await handleAnnounceActivity(activity, tx);
			break;
		case 'EmojiReact':
			await handleEmojiReactActivity(activity, tx);
			break;
		case 'Follow':
			await handleFollowActivity(activity, tx);
			break;
		case 'Like':
			await handleLikeActivity(activity, tx);
			break;
		default:
			console.error(activity);
	}
}

async function processActivity(activity: any) {
	const session = neo4j.session();
	try {
		await session.writeTransaction(async (tx) => {
			await storeActor(activity, tx);
			await handleActivity(activity, tx);
		});
	} finally {
		await session.close();
	}
}

export async function handleInboxPostRequest(c: Context) {
	try {
		const activity = await parseAndValidateActivity(c);

		const valid = await verifySignature(c);
		if (!valid) {
			return new Response(null, { status: 401 });
		}

		await processActivity(activity);

		return new Response(null, { status: 201 });

	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), { status: 400 });
	}
}
