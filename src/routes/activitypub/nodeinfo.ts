import { Context } from 'https://deno.land/x/hono@v3.3.4/context.ts';
import { getJsonResponse } from './get-json-response.ts';

export const nodeinfo = (c: Context) => {
	let data = JSON.stringify({
		'links': [
			{
				'rel': 'http://nodeinfo.diaspora.software/ns/schema/2.0',
				'href': `https://${c.req.header('host')}/nodeinfo/2.0`,
			},
		],
	});

	return getJsonResponse(data);
};
