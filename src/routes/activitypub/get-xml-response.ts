export const getXMLResponse = (data: string) => new Response(data, { headers: { 'Content-Type': 'application/xml' } });
