import { Context } from 'https://deno.land/x/hono@v3.3.4/context.ts';
import { findActorByUsername } from '../../functions/findActorByUsername.ts';
import { runQuery } from './run-query.ts';
import { createOutboxObject } from './create-outbox-object.ts';
import neo4jDriver from '../../neo4j.ts';
import { getNotesByUsername } from '../../functions/getNotesByUsername.ts';
const driver = neo4jDriver;

export async function handleOutboxRequest(c: Context) {
	const session = driver.session();
	const username = c.req.param('username');
	const actor = await findActorByUsername(username);
	const page = c.req.param('page') ? parseInt(c.req.param('page')) : 1;
	const pageSize = 10;
	const skip = (page - 1) * pageSize;

	const items = await getNotesByUsername(username);

	const outbox = createOutboxObject(actor, items, page);
	return new Response(JSON.stringify(outbox), { headers: { 'Content-Type': 'application/json' } });
}
