import { Context } from 'https://deno.land/x/hono@v3.3.4/context.ts';
import { getErrorResponse } from './get-error-response.ts';
import { findActorByUsername } from '../../functions/findActorByUsername.ts';
import { getXMLResponse } from './get-xml-response.ts';

export const webfinger = async (c: Context) => {
	const resourceArray = c.req.queries('resource');
	let type = '', name = '';

	if (resourceArray && resourceArray.length > 0) {
		const resource = resourceArray[0];
		if (resource.includes(':')) [type, name] = resource.split(':');
	}

	if (type !== 'acct') return getErrorResponse();

	const account = await findActorByUsername(name);

	if (account) {
		const domain = c.req.header('host');
		const splitName = name.split('@')[0];
		const baseURL = `https://${domain}/users/${splitName}`;

		const xml = `<?xml version="1.0" encoding="UTF-8"?>
            <XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
                <Subject>${type}:${name}</Subject>
                <Alias>${baseURL}</Alias>
                <Link href="${baseURL}" rel="http://webfinger.net/rel/profile-page" type="text/html"/>
                <Link href="${baseURL}" rel="self" type="application/activity+json"/>
                <Link href="${baseURL}" rel="self" type="application/ld+json; profile="https://www.w3.org/ns/activitystreams"/>
            </XRD>`;

		return getXMLResponse(xml);
	} else {
		return getErrorResponse();
	}
};
