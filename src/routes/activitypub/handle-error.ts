export function handleError(error: any): Response {
	return new Response(JSON.stringify({ error: error.toString() }), { status: 500 });
}
