export function createActorActivityPub(actorUrl: string, properties: any) {
	return {
		'@context': 'https://www.w3.org/ns/activitystreams',
		'type': 'Person',
		'id': actorUrl,
		'preferredUsername': properties.preferredUsername,
		'name': properties.display_name,
		'inbox': `${actorUrl}/inbox`,
		'outbox': `${actorUrl}/outbox`,
		'followers': `${actorUrl}/followers`,
		'following': `${actorUrl}/following`,
		'endpoints': {
			'sharedInbox': `${actorUrl}/inbox`,
		},
	};
}
