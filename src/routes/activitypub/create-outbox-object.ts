export function createOutboxObject(actor: any, items: any[], page: number) {
	const startsWith = actor.id ? new URL(actor.id).host : '';
	return {
		'@context': 'https://www.w3.org/ns/activitystreams',
		id: `http://localhost:8000/${actor.properties.preferredUsername}/outbox`,
		type: 'OrderedCollectionPage',
		partOf: `https://${startsWith}/${actor.properties.preferredUsername}/outbox`,
		next: `https://${startsWith}/outbox/${actor.properties.preferredUsername}/${page + 1}`,
		prev: page > 1 ? `https://${startsWith}/outbox/${actor.properties.preferredUsername}/${page - 1}` : null,
		orderedItems: items,
	};
}
