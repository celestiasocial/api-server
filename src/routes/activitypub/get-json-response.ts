export const getJsonResponse = (data: string) => new Response(data, { headers: { 'Content-Type': 'application/json' } });
