import { Context } from 'https://deno.land/x/hono@v3.3.4/context.ts';
import { getXMLResponse } from './get-xml-response.ts';

export const hostMeta = (c: Context) => {
	let data =
		`<?xml version="1.0" encoding="UTF-8"?>
            <XRD
                xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
                <Link rel="lrdd" template="https://${c.req.header('host')}/.well-known/webfinger?resource={uri}"/>
            </XRD>`;

	return getXMLResponse(data);
};
