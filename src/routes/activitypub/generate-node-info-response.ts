import { Context } from 'https://deno.land/x/hono@v3.3.4/context.ts';

export const generateNodeInfoResponse = (c: Context, version: string) => {
	let data = {
		'version': version,
		'software': {
			'name': 'Celestia Social',
			'version': '1.0.0',
		},
		'protocols': [
			'activitypub',
		],
		'services': {
			'outbound': [],
			'inbound': [],
		},
		'usage': {
			'users': {
				'total': 0,
				'activeMonth': 0,
				'activeHalfyear': 0,
			},
			'localPosts': 0,
		},
		'openRegistrations': true,
		'metadata': {},
	};

	return new Response(JSON.stringify(data), {
		headers: {
			'Content-Type': 'application/json',
		},
	});
};
