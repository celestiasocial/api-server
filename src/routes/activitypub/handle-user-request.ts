import { Context } from 'https://deno.land/x/hono@v3.3.4/context.ts';
import { findActorByUsername } from '../../functions/findActorByUsername.ts';
import { createActorActivityPub } from './create-actor-activity-pub.ts';
import { respondWithError } from './respond-with-error.ts';

export async function handleUserRequest(c: Context) {
	const username = c.req.param('username');
	const actor = await findActorByUsername(username);

	if (!actor) {
		return respondWithError('User not found', 404);
	}

	const actorUrl = actor.properties.id;
	const actorForActivityPub = createActorActivityPub(actorUrl, actor.properties);

	return new Response(JSON.stringify(actorForActivityPub));
}
