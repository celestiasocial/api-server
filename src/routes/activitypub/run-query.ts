import neo4jDriver from '../../neo4j.ts';
import { transformActorToActivityPubAccount } from '../../functions/transformActorToActivityPubAccount.ts';
const driver = neo4jDriver;
export async function runQuery(query: string, actorAUsername: string) {
	const session = driver.session();
	try {
		const result = await session.run(query, { username: actorAUsername });
		const actors = result.records.map((record: any) => {
			const actorBNode = record.get('b');
			const actorBUsername = actorBNode.properties.preferredUsername;
			const domain = new URL(actorBNode.properties.id).host;
			return transformActorToActivityPubAccount(actorBUsername, domain);
		});
		return actors;
	} catch (error: any) {
		console.log(error);
		throw new Error(error.toString());
	} finally {
		await session.close();
	}
}

