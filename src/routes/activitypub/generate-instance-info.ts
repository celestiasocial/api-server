import { Context } from 'https://deno.land/x/hono@v3.3.4/context.ts';

export const generate_instance_info = (c: Context): Response => {
	const domain = c.req.header('host');
	const url = new URL(c.req.url);
	const protocol = url.protocol;

	const instanceData = {
		avatar_upload_limit: 2000000,
		background_image: '',
		background_upload_limit: 4000000,
		banner_upload_limit: 4000000,
		description: `Instance powered by Celestia Social. An experimental fediverse server.`,
		email: 'defaultcontactaddress@celestia.social',
		languages: ['en'],
		max_toot_chars: 10000,
		poll_limits: {
			max_expiration: 31536000,
			max_option_chars: 200,
			max_options: 20,
			min_expiration: 0,
		},
		registrations: true,
		stats: {
			domain_count: 0,
			status_count: 0,
			user_count: 0,
		},
		thumbnail: '',
		title: 'Celestia Social: An experimental fediverse server',
		upload_limit: 16000000,
		uri: `${protocol}//${domain}`,
		urls: {
			streaming_api: `wss://${domain}`,
		},
		version: '4.1.6 (compatible; Mastodon 4.1.6+soapbox)',
	};

	return new Response(JSON.stringify(instanceData), {
		headers: { 'Content-Type': 'application/json' },
	});
};
