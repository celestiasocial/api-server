import { Context } from 'https://deno.land/x/hono@v3.3.4/context.ts';
import { runQuery } from './run-query.ts';
import { handleError } from './handle-error.ts';

export async function handleFollowingRequest(c: Context): Promise<Response> {
	const username = c.req.param('username');
	const followingQuery = 'MATCH (a:Actor {preferredUsername: $username})-[:FOLLOWS]->(b:Actor) RETURN b';

	try {
		const following = await runQuery(followingQuery, username);
		return new Response(JSON.stringify(following));
	} catch (error: any) {
		return handleError(error);
	}
}
