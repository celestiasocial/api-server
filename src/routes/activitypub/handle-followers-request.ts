import { Context } from 'https://deno.land/x/hono@v3.3.4/context.ts';
import { runQuery } from './run-query.ts';
import { handleError } from './handle-error.ts';

export async function handleFollowersRequest(c: Context): Promise<Response> {
	const username = c.req.param('username');
	const followersQuery = 'MATCH (a:Actor {preferredUsername: $username})<-[:FOLLOWS]-(b:Actor) RETURN b';

	try {
		const followers = await runQuery(followersQuery, username);
		return new Response(JSON.stringify(followers));
	} catch (error: any) {
		return handleError(error);
	}
}
