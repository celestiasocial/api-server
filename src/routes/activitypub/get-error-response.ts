export const getErrorResponse = () => new Response('Record not found!', { status: 404 });
