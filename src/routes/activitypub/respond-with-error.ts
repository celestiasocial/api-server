export function respondWithError(message: string, status: number = 500) {
	return new Response(JSON.stringify({ error: message }), { status });
}
