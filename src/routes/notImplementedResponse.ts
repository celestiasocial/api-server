// Define Placeholder Route
import { Context } from 'https://deno.land/x/hono@v3.3.4/context.ts';

export const notImplementedResponse = (): Response => {
	return new Response(JSON.stringify({ 'error': { 'message': 'API Endpoint Not Implemented!' } }), { status: 404 });
};
