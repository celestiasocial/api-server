import { S3 } from "https://deno.land/x/s3@0.5.0/mod.ts";
import * as multiparser from "https://deno.land/x/multiparser@0.114.0/mod.ts";
import uuid62 from 'npm:uuid62';
import neo4j from '../../neo4j.ts';
import { getPayloadFromContext } from "../../index.ts";
import { Hono, Context } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import { config } from 'https://deno.land/x/dotenv/mod.ts';
await config({export: true});
import { findOrCreateActor } from '../../functions/findOrCreateActor.ts';

const driver = neo4j;
const s3 = new S3({
	accessKeyID: Deno.env.get('R2_ACCESS_KEY'),
	secretKey: Deno.env.get('R2_SECRET_KEY'),
	region: Deno.env.get('R2_REGION'),
	endpointURL: Deno.env.get('R2_ENDPOINT_URL'),
});
const bucket = s3.getBucket(Deno.env.get('R2_BUCKET_NAME'));

export const upload_media_route = async (c: Context) => {
		const username = getPayloadFromContext(c.req).user.username;
		const actor = await findOrCreateActor(username);

		const objectName = uuid62.v4();

		const form = await multiparser.multiParser(c.req);
		if (!form.files || Object.keys(form.files).length === 0) {
			throw new Error('No file provided in the request');
		}

		const file = form.files["file"];
		const focus = form.fields.focus;
		const description = form.fields.description;

		await bucket.putObject(objectName, file.content, {
			'Content-Type': 'application/octet-stream',
			acl: 'public-read'
		});

		let url = `${Deno.env.get('R2_CDN_ENDPOINT')}/${objectName}`;

		try {

			// Process response and create media node in Neo4j
			const session = driver.session();
			const result = await session.run(
				`
        MATCH (a:Actor {id: $actorId})
        CREATE (m:Media {
          id: $id,
          description: $description,
          url: $url,
          preview_url: $previewUrl,
          remote_url: $remoteUrl,
          text_url: $textUrl,
          type: $type
        })
        CREATE (a)-[:UPLOADED]->(m)
        `,
				{
					actorId: actor.properties.id,
					id: objectName,
					description: '',
					url: url,
					previewUrl: url, // fill with correct data
					remoteUrl: url, // fill with correct data
					textUrl: url, // fill with correct data
					type: 'application/octet-stream'
				}
			);

			await session.close();
			return new Response(JSON.stringify({
				actorId: actor.properties.id,
				id: objectName,
				description: '',
				url: url,
				previewUrl: url, // fill with correct data
				remoteUrl: url, // fill with correct data
				textUrl: url, // fill with correct data
				type: form.fields.type
			}));
		} catch (error) {
			return new Response(JSON.stringify({error: 'Failed to upload media!'}),
				{ status: 400 })
		}
	};
