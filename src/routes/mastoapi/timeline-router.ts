import { Hono, Context } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import { Neo4jQueryBuilder } from '../../Neo4jQueryBuilder.ts';
import { getActorFromContext } from '../../index.ts';
import { formatMastodonStatus } from '../../functions/formatMastodonStatus.ts';
import { getActorFromNote } from '../../functions/getActorFromNote.ts';

export const home_timeline_route = async (c: Context) => {
	try {
		const user = await getActorFromContext(c.req);
		const queryBuilder = Neo4jQueryBuilder.getInstance();

		const result1 = await queryBuilder.query()
			.custom(`MATCH (a:Actor {objectId: $objectId})`)
			.custom(`MATCH (a)-[:FOLLOWS]->(f:Actor)-[:CREATED]->(n:Note)`)
			.return(['n'])
			.orderBy(['n.published'], 'DESC')
			.execute({ objectId: user.objectId });
	  
		const result2 = await queryBuilder.query()
			.custom(`MATCH (a:Actor)-[:CREATED]->(n:Note)`)
			.return(['a', 'n'])
			.orderBy(['n.published'], 'DESC')
			.limit(100)
			.execute();
	  
	  
		const result = [...result1, ...result2];

		const promises = result.map(async (record: any) => {
			const note = record.get('n');
			const actor = await getActorFromNote(note);
			note.account = actor;
			return formatMastodonStatus(note, user.preferredUsername);
		});

		const statuses = await Promise.all(promises);

		return new Response(JSON.stringify(statuses), {
			headers: { 'Content-Type': 'application/json' },
		});
	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), {
			status: 500,
			headers: { 'Content-Type': 'application/json' },
		});
	}
};


export const public_timeline_route = async (c: Context) => {
	const notes = await getLatestNotes(c);
	return new Response(JSON.stringify(notes), {
		headers: { 'Content-Type': 'application/json' },
	});
};

const getLatestNotes = async (c: Context) => {
	const queryBuilder = Neo4jQueryBuilder.getInstance();
	try {
		const result = await queryBuilder.query()
			.match('(a:Actor)-[:CREATED]->(n:Note)')
			.return(['a', 'n'])
			.orderBy(['n.published'], 'DESC')
			.limit(100)
			.execute();

		const notes = result.map((record: any) => {
			const note = record.get('n');
			const actor = record.get('a');
			note.account = actor;
			return formatMastodonStatus(note, actor.properties.preferredUsername);
		});

		// Await all promises to resolve
		const formattedNotes = await Promise.all(notes);

		return formattedNotes;
	} catch (error: any) {
		console.log(error);
	}
};
