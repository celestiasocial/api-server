import { Context } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import { getPayloadFromContext } from '../../index.ts';
import { z } from 'https://deno.land/x/zod@v3.21.4/mod.ts';
import jwt from 'https://esm.sh/jsonwebtoken@9.0.1';
import {
	createPasswordHash,
} from 'https://gitlab.com/elainejackson/cf-workers-password-hasher/-/raw/main/cf-workers-password-hasher.js';
import { load } from 'https://deno.land/std@0.196.0/dotenv/mod.ts';
import neo4j from '../../neo4j.ts';
import { generateKeyPair } from 'https://gitlab.com/soapbox-pub/fedisign/-/raw/v0.2.1/mod.ts';
import { transformActorToAccount } from '../../functions/transformActorToAccount.ts';
import uuid62 from 'npm:uuid62';

const env = await load();

const driver = neo4j;

export const registerAccountRoute = async (c: Context): Promise<Response> => {
	try {
		const body = await c.req.json();
		const {agreement, password, username, email} = body;
		checkRequiredFields(agreement, password, username, email);

		const hashedPassword = await createPasswordHash(password);
		const keyPair = await generateKeyPair();

		const session = driver.session();
		await createUserAccount({username, email}, session, hashedPassword, keyPair);

		const tokenResponse = await issueOAuthToken({username, email});

		return new Response(JSON.stringify(tokenResponse), {
			headers: { 'Content-Type': 'application/json' }
		});

	} catch (error) {
		return new Response(JSON.stringify({ message: error.message }), {
			headers: { 'Content-Type': 'application/json' },
			status: error.message === 'Required fields are missing' || error.message === 'Username or email already exists' ? 400 : 500
		});
	}
};

export const updateCredentialsRoute = async (c: Context): Promise<Response> => {
	let body;

	const contentType = c.req.header('content-type') || '';
	if (contentType === 'application/json') {
		body = await c.req.json();
	} else if (contentType.includes('multipart/form-data')) {
		body = await c.req.parseBody();
	} else {
		return new Response(JSON.stringify({ error: 'Unsupported content type' }), {
			status: 415,
			headers: { 'Content-Type': 'application/json' },
		});
	}

	try {
		const validatedBody = updateCredentialsSchema.parse(body);
		const username = getPayloadFromContext(c.req)?.user?.username;

		if (!username) {
			throw new Error('Error fetching the logged-in user\'s account!')
		}

		const updatedUser = transformActorToAccount(await updateUserDetails(username, validatedBody));

		return new Response(JSON.stringify(updatedUser), {
			headers: { 'Content-Type': 'application/json' },
		});
	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), {
			status: 400,
			headers: { 'Content-Type': 'application/json' },
		});
	}
};

const checkRequiredFields = (agreement: any, password: any, username: any, email: any) => {
	if (!agreement || !password || !username || !email) {
		throw new Error("Required fields are missing");
	}
};

const createUserAccount = async ({username, email}: any, session: any, hashedPassword: string, keyPair: any) => {
	const actorId = `${Deno.env.get("WEB_SCHEME")}://${Deno.env.get("WEB_DOMAIN")}/${username}`;
	const objectId = uuid62.v4();

	const userExists = await session.run(
		"MATCH (a:Actor) WHERE a.preferredUsername = $username OR a.email = $email RETURN a",
		{username, email}
	);

	if (userExists.records.length > 0) {
		throw new Error("Username or email already exists");
	}

	const privateKey = arrayBufferToBase64(await crypto.subtle.exportKey("pkcs8", keyPair.privateKey));
	const publicKey = arrayBufferToBase64(await crypto.subtle.exportKey("spki", keyPair.publicKey));

	return await session.run(
		"MERGE (a:Actor {id: $actorId}) ON CREATE SET a += {type: 'Person', preferredUsername: $username, name: $username, summary: '', email: $email, objectId: $objectId, privateKey: $privateKey, publicKey: $publicKey, password: $hashedPassword} RETURN a",
		{
			actorId,
			objectId,
			username,
			email,
			privateKey,
			publicKey,
			hashedPassword
		}
	);
};

const issueOAuthToken = async ({username, email}: any) => {
	const payload = {
		user: {
			username,
			email,
		},
		nbf: Math.floor(Date.now() / 1000) - (60 * 60),
		exp: Math.floor(Date.now() / 1000) + (24 * (60 * 60))
	};
	const token = await jwt.sign(payload, env["JWT_SECRET"]);

	return {
		access_token: token,
		token_type: 'Bearer'
	};
};

const ActorType = z.enum(["Application", "Group", "Organization", "Person", "Service"]);
const VisibilityScope = z.enum(["public", "unlisted", "local", "private", "direct", "list"]);

const AccountAttributeField = z.object({
	name: z.string(),
	value: z.string().optional(),
});

const updateCredentialsSchema = z.object({
	accepts_chat_messages: z.union([z.boolean(), z.string(), z.number()]).optional(),
	actor_type: ActorType.optional(),
	allow_following_move: z.union([z.boolean(), z.string(), z.number()]).optional(),
	also_known_as: z.array(z.string()).optional(),
	avatar: z.string().optional(),
	birthday: z.string().optional(),
	bot: z.union([z.boolean(), z.string(), z.number()]).optional(),
	default_scope: VisibilityScope.optional(),
	discoverable: z.union([z.boolean(), z.string(), z.number()]).optional(),
	display_name: z.string().optional(),
	fields_attributes: z.array(AccountAttributeField).or(z.object({})).optional(),
	header: z.string().optional(),
	hide_favorites: z.union([z.boolean(), z.string(), z.number()]).optional(),
	hide_followers: z.union([z.boolean(), z.string(), z.number()]).optional(),
	hide_followers_count: z.union([z.boolean(), z.string(), z.number()]).optional(),
	hide_follows: z.union([z.boolean(), z.string(), z.number()]).optional(),
	hide_follows_count: z.union([z.boolean(), z.string(), z.number()]).optional(),
	locked: z.union([z.boolean(), z.string(), z.number()]).optional(),
	no_rich_text: z.union([z.boolean(), z.string(), z.number()]).optional(),
	note: z.string().optional(),
	pleroma_background_image: z.string().optional(),
	pleroma_settings_store: z.object({}).optional(),
	show_birthday: z.union([z.boolean(), z.string(), z.number()]).optional(),
	show_role: z.union([z.boolean(), z.string(), z.number()]).optional(),
	skip_thread_containment: z.union([z.boolean(), z.string(), z.number()]).optional(),
});

function arrayBufferToBase64(buffer: Iterable<number> | ArrayBuffer) {
	let binary = '';
	let bytes: Uint8Array;

	if (buffer instanceof ArrayBuffer) {
		bytes = new Uint8Array(buffer);
	} else {
		// Convert Iterable<number> to Uint8Array
		bytes = new Uint8Array(Array.from(buffer));
	}

	const len = bytes.byteLength;
	for (let i = 0; i < len; i++) {
		binary += String.fromCharCode(bytes[i]);
	}
	return btoa(binary);
}

const updateUserDetails = async (username: string, body: any) => {
	const session = driver.session();
	const actorId = `${Deno.env.get('WEB_SCHEME')}://${Deno.env.get('WEB_DOMAIN')}/${username}`;

	// Prepare the properties to be set, flattening the nested objects
	const properties = {
		display_name: body.display_name,
		note: body.note,
		locked: body.locked,
		bot: body.bot,
		discoverable: body.discoverable,
		fields_attributes: JSON.stringify(body.fields_attributes),  // Convert to JSON string
		source_privacy: body.source?.privacy,
		source_sensitive: body.source?.sensitive,
		source_language: body.source?.language,
	};

	let updatedActor;
	try {
		const result = await session.run(
			'MATCH (a:Actor {id: $actorId}) SET a += $properties RETURN a',
			{ actorId, properties },
		);

		if (result.records.length === 0) {
			throw new Error('Actor not found');
		}

		updatedActor = result.records[0].get('a');
	} catch (error) {
		throw new Error(`Failed to update user details: ${error.message}`);
	} finally {
		await session.close();
	}

	return updatedActor;
};

