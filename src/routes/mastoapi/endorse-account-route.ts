import { getPayloadFromContext } from "../../index.ts";
import { createRelationshipResponse } from "./createRelationshipResponse.ts";

export const endorse_account_route = async (c: Context) => {
	try {
		// Extract the current user's ID and the account ID to be endorsed
		const currentUserId = await getPayloadFromContext(c.req).user.id;
		const urlParamId = c.req.param('id');

        if (!currentUserId || !urlParamId) {
            throw new Error('An error occured while fetching account information'!);
        }

		// Call a function to create the relationship
		// Note: You would need to implement this function based on your specific needs
		const relationship = createRelationshipResponse(currentUserId, urlParamId);

		// Return the created relationship
		return new Response(JSON.stringify(relationship), {
			headers: { 'Content-Type': 'application/json' },
		});
	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), {
			status: 500,
			headers: { 'Content-Type': 'application/json' },
		});
	}
};
