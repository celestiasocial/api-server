import { Neo4jQueryBuilder } from '../../Neo4jQueryBuilder.ts';
import { Context, Hono } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import { getActorFromContext } from '../../index.ts';
import { transformPollToMastodonPoll } from '../../functions/transformPollToMastodonPoll.ts';
import { votePayloadSchema } from '../../schemas/vote-payload-schema.ts';
import { getPollById } from '../../functions/get-poll-by-id.ts';
import uuid62 from 'npm:uuid62';


export const get_poll_route =	async (c: Context) => {
		try {
			const pollId = c.req.param('id');
			const actor = await getActorFromContext(c.req);  // Fetch the actor (user) from the context
			const username = actor.preferredUsername;

			const poll = await getPollById(pollId);
			const mastodonPoll = await transformPollToMastodonPoll(poll, username);

			return new Response(JSON.stringify(mastodonPoll), {
				headers: { 'Content-Type': 'application/json' },
			});

		} catch (error) {
			return new Response(JSON.stringify({ error: error.message }), {
				status: 500,
				headers: { 'Content-Type': 'application/json' },
			});
		}
	};

	export const cast_vote_route = async (c: Context) => {
		try {
			const actor = await getActorFromContext(c.req);
			const data = await c.req.json();
			const pollId = c.req.param('id');
	
			// Validate the payload
			const result = votePayloadSchema.safeParse(data);
			if (!result.success) {
				throw new Error('Invalid data');
			}
			const choices = result.data.choices.map(choice => parseInt(choice));
	
			// Fetch the poll by id
			const poll = await getPollById(pollId);
			if (!poll) {
				throw new Error('Poll not found');
			}
	
			// Validate choices
			choices.forEach(choice => {
				if (choice < 0 || choice >= poll.options.length) {
					throw new Error('Invalid vote choice');
				}
			});
	
			// If poll.multiple is false, ensure only one vote is being cast
			if (!poll.multiple && choices.length > 1) {
				throw new Error('Only one vote allowed for this poll');
			}
	
			const queryBuilder = Neo4jQueryBuilder.getInstance().query();
	
			
			// Delete all previous votes by the user for this poll
			queryBuilder
			.match('u', 'Actor')
			.where('u.preferredUsername = $username', { username: actor.preferredUsername })
			.match('p', 'Poll')
			.where('p.objectId = $pollId', { pollId })
			.with('u, p')
			.match('(u)-[r1:VOTED]->(v:Vote)-[:IN_POLL]->(p)')
			.delete([], ['r1', 'v']); // Delete relationship 'r1' and node 'v'

			await queryBuilder.execute();

			// Create a new vote for each choice and establish the relationships in the database

			for (const choice of choices) {
				const objectId = uuid62.v4();
			
				queryBuilder
					.match('u', 'Actor')
					.where('u.preferredUsername = $username', { username: actor.preferredUsername })
					.match('p', 'Poll')
					.where('p.objectId = $pollId', { pollId })
					.merge('vote', 'Vote', { objectId })
					.custom(`SET vote.choice = toInteger($choice) `)
					.merge('(u)-[:VOTED]->(vote)')
					.merge('(vote)-[:IN_POLL]->(p)')
					.return(['vote']);
			
				await queryBuilder.executeWithParams({ username: actor.preferredUsername, pollId, objectId, choice }); // Pass all parameters here
			}
					

	queryBuilder.close();

	// Fetch the updated poll and return it
	const updatedPoll = await getPollById(pollId);
	return new Response(JSON.stringify(updatedPoll), {
	headers: { 'Content-Type': 'application/json' },
	});

	} catch (error) {
	return new Response(JSON.stringify({ error: error.message }), {
	status: 500,
	headers: { 'Content-Type': 'application/json' },
	});
	}
};