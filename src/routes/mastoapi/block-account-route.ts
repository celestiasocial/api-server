import { getPayloadFromContext } from "../../index.ts";
import neo4jDriver from "../../neo4j.ts";
import { createRelationshipResponse } from "./createRelationshipResponse.ts";
const driver = neo4jDriver;

export const block_account_route = async (c: Context) => {
	try {
		// Extract the current user's ID and the account ID to be blocked
		const currentUserId = await getPayloadFromContext(c.req).user.id;
		const urlParamId = c.req.param('id');

		// Call a function to block the account
		// Note: You would need to implement this function based on your specific needs
		const block = await blockAccount(currentUserId, urlParamId);

		// Return the block status
		return createRelationshipResponse(urlParamId, currentUserId);
	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), {
			status: 500,
			headers: { 'Content-Type': 'application/json' },
		});
	}
};

const blockAccount = async (currentUserId: string, urlParamId: string) => {
	const session = driver.session();
	let result;
	try {
		result = await session.writeTransaction(txc =>
			txc.run(
				"MATCH (a:Actor {objectId: $currentUserId}), (b:Actor {objectId: $urlParamId}) " +
				"MERGE (a)-[r:BLOCKED]->(b) " +
				"RETURN r",
				{ currentUserId, urlParamId }
			)
		);

		if (result.records.length === 0) {
			throw new Error('No nodes found to create a relationship');
		}
	} catch (error) {
		console.log(error);
	} finally {
		await session.close();
	}
	return result.records[0].get('r').properties;
};
