import { load } from "https://deno.land/std/dotenv/mod.ts";
import { Context, Hono } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import jwt from 'https://esm.sh/jsonwebtoken@9.0.1';
import uuid62 from 'npm:uuid62';
import { verifyPasswordHash } from 'https://gitlab.com/elainejackson/cf-workers-password-hasher/-/raw/main/cf-workers-password-hasher.js';
const env = await load();
import neo4j from '../../neo4j.ts';

import { transformActorToAccount } from '../../functions/transformActorToAccount.ts';
const driver = neo4j;

export const oauth_authorize_form_route = (c) => {
		const url = new URL(c.req.url);
		const params = url.searchParams;

		return new Response(`
        <style>
            body {
                font-family: 'Whitney', sans-serif;
                background-color: #36393f;
                color: #dcddde;
                display: flex;
                align-items: center;
                justify-content: center;
                height: 100vh;
                margin: 0;
            }
            form {
                background: #202225;
                width: 300px;
                padding: 20px;
                border-radius: 5px;
            }
            input {
                display: block;
                width: 100%;
                padding: 10px;
                margin-bottom: 10px;
                background: #40444b;
                border: none;
                border-radius: 3px;
                color: #dcddde;
            }
            button {
                display: block;
                width: 100%;
                padding: 10px;
                border: none;
                border-radius: 3px;
                color: #dcddde;
                background-color: #7289da;
                cursor: pointer;
                font-weight: bold;
            }
            button:hover {
                background-color: #677bc4;
            }
        </style>
        <form action="/oauth/authorize" method="post">
        		<h4>Celestia Social: Login Required</h4>
            <input type="text" name="username" placeholder="Email">
            <input type="password" name="password" placeholder="Password">
            <input type="hidden" name="client_id" value="${params.get('client_id')}">
            <input type="hidden" name="redirect_uri" value="${params.get('redirect_uri')}">
            <input type="hidden" name="response_type" value="${params.get('response_type')}">
            <input type="hidden" name="scope" value="${params.get('scope')}">
            <button type="submit">Login</button>
        </form>
    `, { headers: { 'Content-Type': 'text/html' } });
	};

	export const oauth_authorize_route = async (c: Context) => {
		const formData = await c.req.formData();
		const username = formData.get('username') || '';
		const password = formData.get('password') || '';
		const client_id = formData.get('client_id') || '';
		const redirect_uri = formData.get('redirect_uri') || '';
		const response_type = formData.get('response_type') || '';
		const scope = formData.get('scope') || '';

		const loginForm = (errorMessage = '', values: any = {}) => `
        <style>
            body {
                font-family: 'Whitney', sans-serif;
                background-color: #36393f;
                color: #dcddde;
                display: flex;
                align-items: center;
                justify-content: center;
                height: 100vh;
                margin: 0;
            }
            form {
                background: #202225;
                width: 300px;
                padding: 20px;
                border-radius: 5px;
            }
            input {
                display: block;
                width: 100%;
                padding: 10px;
                margin-bottom: 10px;
                background: #40444b;
                border: none;
                border-radius: 3px;
                color: #dcddde;
            }
            button {
                display: block;
                width: 100%;
                padding: 10px;
                border: none;
                border-radius: 3px;
                color: #dcddde;
                background-color: #7289da;
                cursor: pointer;
                font-weight: bold;
            }
            button:hover {
                background-color: #677bc4;
            }
            .error {
                color: #e74c3c;
                margin-bottom: 10px;
            }
        </style>
        <form action="/oauth/authorize" method="post">
            <h4>Celestia Social: Login Required</h4>
            ${errorMessage ? `<p class="error">${errorMessage}</p>` : ''}
            <input type="text" name="username" placeholder="Email" value="${values.username || ''}">
            <input type="password" name="password" placeholder="Password">
            <input type="hidden" name="client_id" value="${values.client_id || ''}">
            <input type="hidden" name="redirect_uri" value="${values.redirect_uri || ''}">
            <input type="hidden" name="response_type" value="${values.response_type || ''}">
            <input type="hidden" name="scope" value="${values.scope || ''}">
            <button type="submit">Login</button>
        </form>
    `;

		try {
			// Try to log in with the provided username and password
			let user = await login(username, password); // Assuming login function is defined elsewhere
			// If the login is successful, generate the authorization code
			const authCode = await createToken(username); // Assuming createToken function is defined elsewhere

			if (!redirect_uri) {
				return new Response(loginForm('Invalid redirect_uri.', formData), { headers: { 'Content-Type': 'text/html' } });
			}

			// redirect to the redirect_uri with the authorization code
			if (redirect_uri === 'urn:ietf:wg:oauth:2.0:oob') {
				// Render the authorization code on a webpage
				return new Response(`
            <style>
                body {
                    font-family: 'Whitney', sans-serif;
                    background-color: #36393f;
                    color: #dcddde;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    height: 100vh;
                    margin: 0;
                }
                .auth-code {
                    background: #202225;
                    width: 300px;
                    padding: 20px;
                    border-radius: 5px;
                    margin-top: 20px;
                }
            </style>
            <div class="auth-code">
                <h4>Your Authorization Code</h4>
                <p>${authCode}</p>
            </div>
            `, { headers: { 'Content-Type': 'text/html' } });
			} else {
				// Redirect to the redirect_uri with the authorization code
				const url = `${redirect_uri}?code=${authCode}`;
				return new Response(`<!DOCTYPE html> <html> <head> <meta http-equiv="refresh" content="0; URL=${url}"> </head> <body> <p>This page will be redirected instantly...</p> </body> </html>`, {
					status: 302,
					headers: {
						Location: url,
						'Content-Type': 'text/html',
					},
				});
			}

		} catch (error: any) {
			// If there is an error (e.g. user not found or password is incorrect), respond with an error message
			return new Response(loginForm(error.message, formData), { headers: { 'Content-Type': 'text/html' } });
		}
	};

	export const oauth_token_route = async (c: Context) => {
		try {
		  let formData: { [key: string]: any };
		  const contentType = c.req.header('Content-Type');
	  
		  if (contentType && contentType.includes('application/json')) {
			formData = await c.req.json();
		  } else {
			formData = await c.req.parseBody();
		  }
	  
		  const code = formData['code'] || '';
		  const username = formData['username'];
		  const password = formData['password'];
	  
		  if (code) {
			// Search for the Token in Neo4j using the code
			const session = driver.session();
			const result = await session.readTransaction(
			  (tx: any) => tx.run(
				'MATCH (tok:Token { id: $code })-[:CREATED]-(actor:Actor) RETURN tok, actor',
				{ code }
			  )
			);
			session.close();
	  
			const record = result.records[0];
			if (record) {
			  const actor = record.get('actor').properties;
			  
			  // Constructing the payload including the actor
			  const payload = {
				user: {
					username: actor.username
				},
				nbf: Math.floor(Date.now() / 1000) - (60 * 60),
				exp: Math.floor(Date.now() / 1000) + (24 * (60 * 60)),
			  };
			  const token = await jwt.sign(payload, Deno.env.get('JWT_SECRET'));
	  
			  return new Response(JSON.stringify({
				access_token: token,
				token_type: 'Bearer'
			  }), {
				headers: { 'Content-Type': 'application/json' },
				status: 200,
			  });
			} else {
			  return new Response(JSON.stringify({
				error: 'Invalid code'
			  }), {
				headers: { 'Content-Type': 'application/json' },
				status: 401
			  });
			}
		  } else if (username && password) {
			const actor = await login(username, password);
	  
			if (actor) {
				const payload = {
					user: {
						username: actor.username
					},
					nbf: Math.floor(Date.now() / 1000) - (60 * 60),
					exp: Math.floor(Date.now() / 1000) + (24 * (60 * 60)),
				  };
			  const token = await jwt.sign(payload, Deno.env.get('JWT_SECRET'));
	  
			  return new Response(JSON.stringify({
				access_token: token,
				token_type: 'Bearer'
			  }), {
				headers: { 'Content-Type': 'application/json' },
				status: 200,
			  });
			} else {
			  return new Response(JSON.stringify({
				error: 'Invalid login credentials'
			  }), {
				headers: { 'Content-Type': 'application/json' },
				status: 401
			  });
			}
		  } else {
			return new Response(JSON.stringify({
			  error: 'Missing code or username and password'
			}), {
			  headers: { 'Content-Type': 'application/json' },
			  status: 400
			});
		  }
		} catch (error: any) {
		  console.log(error);
		  return new Response(JSON.stringify({
			error: 'An error occurred'
		  }), {
			headers: { 'Content-Type': 'application/json' },
			status: 500
		  });
		}
	  };
	  
const login = async (username: string, password: string) => {
	const session = driver.session();

	try {
		const result = await session.run(
			"MATCH (a:Actor {preferredUsername: $username}) RETURN a",
			{ username }
		);

		if (result.records.length === 0) {
			return false;
		}

		const actor = result.records[0].get('a');

		if (!actor.properties.password) {
			return false;
		}

		const isPasswordValid = await verifyPasswordHash(password, actor.properties.password);

		if (!isPasswordValid) {
			return false;
		}

		return transformActorToAccount(actor);
	} finally {
		await session.close();
	}
};

/**
 *
 * @param {string} username
 * @param {string} email
 * @return {string} token
 */
async function createToken(identifier: string) {
	try {
	  const token = uuid62.v4();
	  const session = driver.session();
	  const isEmail = identifier.includes('@');
  
	  let query = '';
	  let params = {};
  
	  if (isEmail) {
		query = `
		  MERGE (actor:Actor {email: $email})
		  CREATE (tok:Token { id: $token})
		  CREATE (actor)-[:CREATED]->(tok)
		`;
		params = { token, email: identifier };
	  } else {
		query = `
		  MERGE (actor:Actor {username: $username})
		  CREATE (tok:Token { id: $token})
		  CREATE (actor)-[:CREATED]->(tok)
		`;
		params = { token, username: identifier };
	  }
  
	  await session.writeTransaction(
		(tx: any) => tx.run(query, params)
	  );
	  return token;
	} catch (error) {
	  console.error('Error creating token:', error);
	  throw error;
	}
  }  

/**
 *
 * @param {string} token
 * @return {object} {username, email}
 */
async function verifyToken(token: any) {
	try {
		const session = driver.session();
		const result = await session.readTransaction(
			(tx: any) => tx.run(
				`MATCH (tok:Token { id: $token }) RETURN tok`,
				{ token }
			)
		);

		if (result.records.length === 0) {
			throw new Error('Token not found');
		}

		const record = result.records[0].get('tok').properties;
		return { username: record.username, email: record.email };
	} catch (error) {
		console.error('Error verifying token:', error);
		throw error;
	}
}
