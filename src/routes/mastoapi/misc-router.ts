import { Hono } from 'https://deno.land/x/hono@v3.3.4/mod.ts';

export const manifest_route = (c) => {
	const domain = c.req.header('host');
	const url = new URL(c.req.url);
	const protocol = url.protocol;

	const manifestData = {
		categories: ['social'],
		scope: `${protocol}//${domain}`,
		serviceworker: { src: '/sw.js' },
		start_url: '/',
	};

	return new Response(JSON.stringify(manifestData), {
		headers: { 'Content-Type': 'application/json' },
	});
};
