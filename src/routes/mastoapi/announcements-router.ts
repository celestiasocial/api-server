import { Hono, Context } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import { getPayloadFromContext } from '../../index.ts';
import { load } from 'https://deno.land/std@0.196.0/dotenv/mod.ts';
const env = await load();
import neo4j from '../../neo4j.ts';
import { convertNeo4jDateTimeToJSTS } from '../../functions/convertNeo4jDateTimeToJSTS.ts';
const driver = neo4j;

	export const dismiss_announcement_route = async (c: Context) => {
		try {
			const session = driver.session();
			const username = getPayloadFromContext(c.req).user.username;
			const announcementId = c.req.param('id');

			// Use MERGE to avoid creating duplicate relationships
			const result = await session.run(
				"MATCH (a:Actor { preferredUsername: $username }), (n:Announcement { id: $announcementId }) " +
				"MERGE (a)-[:DISMISSED]->(n) " +
				"RETURN a, n",
				{
					username,
					announcementId
				}
			);

			if (result.records.length === 0) {
				throw new Error('Failed to dismiss announcement');
			}

			return new Response(JSON.stringify({ message: 'Announcement dismissed successfully' }), {
				headers: { 'Content-Type': 'application/json' },
			});

		} catch (error) {
			return new Response(JSON.stringify({ error: error.message }), {
				status: 500,
				headers: { 'Content-Type': 'application/json' },
			});
		}
	};

	export const get_announcements_route = async (c: Context) => {
		try {
			const session = driver.session();
			const username = getPayloadFromContext(c.req).user.username;

			// Fetch announcements that the user hasn't dismissed
			const result = await session.run(
				"MATCH (n:Announcement), (a:Actor { preferredUsername: $username }) WHERE NOT (a)-[:DISMISSED]->(n) RETURN n",
				{
					username
				}
			);

			const announcements = result.records.map(record => {
				const properties = record.get('n').properties;

				// Convert neo4j DateTime to JavaScript Date string
				if (properties.published_at) {
					properties.published_at = convertNeo4jDateTimeToJSTS(properties.published_at);
				}

				properties.all_day = true;

				if (properties.updated_at) {
					properties.updated_at = convertNeo4jDateTimeToJSTS(properties.updated_at);
				}

				if (properties.ends_at) {
					properties.ends_at = convertNeo4jDateTimeToJSTS(properties.ends_at);
				}

				return properties;
			});

			return new Response(JSON.stringify(announcements), {
				headers: { 'Content-Type': 'application/json' },
			});
		} catch (error) {
			return new Response(JSON.stringify({ error: error.message }), {
				status: 500,
				headers: { 'Content-Type': 'application/json' },
			});
		}
	};

