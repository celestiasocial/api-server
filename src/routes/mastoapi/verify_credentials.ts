import { Context } from 'https://deno.land/x/hono@v3.3.4/context.ts';
import { findActorByUsername } from '../../functions/findActorByUsername.ts';
import { transformActorToAccount } from '../../functions/transformActorToAccount.ts';
import { getActorFromContext } from '../../index.ts';

export const verify_credentials_route = async (c: Context): Promise<Response> => {
	try {
		const actor = await getActorFromContext(c.req);
		const responseData = transformActorToAccount(actor);
		return new Response(JSON.stringify(responseData), {
			headers: { 'Content-Type': 'application/json' },
		});
	} catch (error) {
		console.log(error);
		return new Response(JSON.stringify({ message: error.message }), {
			headers: { 'Content-Type': 'application/json' },
			status: 401,
		});
	}
};
