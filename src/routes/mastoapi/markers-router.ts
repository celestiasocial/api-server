import { Hono, Context } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import { getPayloadFromContext } from '../../index.ts';
import uuid62 from 'npm:uuid62';
import { load } from 'https://deno.land/std@0.196.0/dotenv/mod.ts';
const env = await load();
import neo4j from '../../neo4j.ts';

const driver = neo4j;

const markersRouter = (router: Hono) => {
	router.get('/api/v1/markers', async (c: Context) => {
		try {
			const session = driver.session();
			const username = getPayloadFromContext(c.req).user.username;
			const timelines = c.req.query('timeline');

			// If no timeline is provided, return an empty object
			if (!timelines || timelines.length === 0) {
				return new Response(JSON.stringify({}), {
					headers: { 'Content-Type': 'application/json' },
				});
			}

			// Fetch the markers for the specified timelines
			const markers = {};
			for (const timeline of timelines) {
				const result = await session.run(
					"MATCH (a:Actor { preferredUsername: $username })-[:MARKS]->(m:Marker { timeline: $timeline }) RETURN m",
					{
						username,
						timeline
					}
				);

				if (result.records.length > 0) {
					// Assume that the marker structure is { id: string, position: number }
					const marker = result.records[0].get('m').properties;
					markers[timeline] = marker;
				}
			}

			return new Response(JSON.stringify(markers), {
				headers: { 'Content-Type': 'application/json' },
			});

		} catch (error) {
			return new Response(JSON.stringify({ error: error.message }), {
				status: 500,
				headers: { 'Content-Type': 'application/json' },
			});
		}
	});

	router.post('/api/v1/markers', async (c: Context) => {
		try {
			const session = driver.session();
			const data = await c.req.json();
			const username = getPayloadFromContext(c.req).user.username;

			// Assume marker structure: { timeline: string, position: number }
			const marker = data.marker;
			if (!marker || typeof marker.position !== 'number' || typeof marker.timeline !== 'string') {
				throw new Error('Invalid marker data');
			}

			// Use MERGE to avoid creating duplicate markers or relationships
			const result = await session.run(
				"MATCH (a:Actor { preferredUsername: $username }) " +
				"MERGE (a)-[r:MARKS]->(m:Marker { id: a.id + '-' + $timeline }) " +
				"ON CREATE SET r.position = $position, m.id = $id, m.timeline = $timeline " +
				"ON MATCH SET r.position = $position " +
				"RETURN a, m",
				{
					username,
					position: marker.position,
					timeline: marker.timeline,
					id: uuid62.v4() // Generate a new id for the marker
				}
			);

			if (result.records.length === 0) {
				throw new Error('Failed to create or update marker');
			}

			return new Response(JSON.stringify({ message: 'Marker created or updated successfully' }), {
				headers: { 'Content-Type': 'application/json' },
			});

		} catch (error) {
			return new Response(JSON.stringify({ error: error.message }), {
				status: 500,
				headers: { 'Content-Type': 'application/json' },
			});
		}
	});
}

export default markersRouter;
