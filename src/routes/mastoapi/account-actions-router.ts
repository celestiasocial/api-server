import { Context } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import { load } from 'https://deno.land/std@0.196.0/dotenv/mod.ts';
import neo4j from '../../neo4j.ts';
import { transformActorToAccount } from '../../functions/transformActorToAccount.ts';
import { findActorByUsername } from '../../functions/findActorByUsername.ts';
import { getActorFromContext } from '../../index.ts';
import { createRelationshipResponse } from './createRelationshipResponse.ts';

const env = await load();
const driver = neo4j;

const jsonResponse = (data: any, status: number = 200) => {
	return new Response(JSON.stringify(data), {
		status: status,
		headers: { 'Content-Type': 'application/json' },
	});
}

export const get_account_relationship = async (c: Context): Promise<Response> => {
	const currentActor = await getActorFromContext(c.req);
	const response = await createRelationshipResponse(
		c.req.query("id[]"),
		currentActor["objectId"]
	);
	return response;
};

export const get_account_by_id_route = async (c: Context): Promise<Response> => {
	try {
		const username = c.req.param("id");
		const actor = await findActorByUsername(username);

		if (!actor) {
			return jsonResponse({ error: 'User not found' }, 404);
		}

		return jsonResponse(transformActorToAccount(actor));
	} catch(error) {
		return jsonResponse({ error: error.message }, 500);
	}
};
