import { Relationship } from '../../interfaces/Relationship.ts';
import neo4jDriver from '../../neo4j.ts';
const driver = neo4jDriver;

export const createRelationshipResponse = async (actorIdOrUsername: string, currentActorId: string): Promise<Response> => {
	const session = driver.session();

	const relationship: Relationship = {
		blocked_by: false,
		blocking: false,
		domain_blocking: false,
		endorsed: false,
		followed_by: false,
		following: false,
		id: actorIdOrUsername,
		muting: false,
		muting_notifications: false,
		note: '',
		notifying: false,
		requested: false,
		showing_reblogs: false,
		subscribing: false,
	};

	try {
		// First check relationships from A to B
		const result = await session.run(
			'MATCH (a:Actor {objectId: $currentActorId})-[r]->(b:Actor) WHERE b.objectId = $actorIdOrUsername OR b.preferredUsername = $actorIdOrUsername RETURN r',
			{ currentActorId, actorIdOrUsername },
		);

		const relationshipTypes = new Set();
		for (const record of result.records) {
			const relationship = record.get('r');
			relationshipTypes.add(relationship.type);
		}

		relationshipTypes.forEach((i) => {
			let relationshipType = i.toLowerCase();
			switch (relationshipType) {
				case 'follows':
					relationship['following'] = true;
					/* falls through */
				case 'blocked':
					relationship['blocking'] = true;
					relationship['following'] = false;
			}
		})

		// Now check relationships from B to A
		const result2 = await session.run(
			'MATCH (a:Actor {objectId: $currentActorId})<-[r]-(b:Actor) WHERE b.objectId = $actorIdOrUsername OR b.preferredUsername = $actorIdOrUsername RETURN r',
			{ currentActorId, actorIdOrUsername },
		);

		const relationshipTypes2 = new Set();
		for (const record of result2.records) {
			const relationship = record.get('r');
			relationshipTypes.add(relationship.type);
		}

		relationshipTypes2.forEach((i) => {
			let relationshipType = i.toLowerCase();
			switch (relationshipType) {
				case 'follows':
					relationship['followed_by'] = true;
					/* falls through */
				case 'blocked':
					relationship['blocked_by'] = true;
					/* falls through */
				default:
					// ignore, not relevent here!
			}
		})

	} catch (error) {
		console.error(error);
	} finally {
		await session.close();
	}

	return new Response(JSON.stringify([relationship]), {
		headers: { 'Content-Type': 'application/json' },
	});
};
