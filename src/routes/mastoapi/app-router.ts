import { Context, Hono } from 'https://deno.land/x/hono@v3.3.4/mod.ts';

const FAKE_APP = {
	id: '1',
	name: 'Celestia',
	website: null,
	redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
	client_id: 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', // she cry
	client_secret: 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', // 😱 😱 😱
	vapid_key: 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=',
};

export const app_router_post = async (c) => {
	let requestBody;
	let redirect_uri = FAKE_APP.redirect_uri;

	// Try to parse request body
	try {
		requestBody = await c.req.json();
		if (requestBody.redirect_uris) {
			redirect_uri = requestBody.redirect_uris;
		}
	} catch (_e) {
		// If an error occurs while parsing the request body, ignore it and return the default FAKE_APP object
	}

	return new Response(JSON.stringify({
		...FAKE_APP,
		redirect_uri: redirect_uri,
	}), {
		headers: {
			'Content-Type': 'application/json',
		}
	});
};

const appRouter = (router: Hono) => {
	router.get('/api/v1/apps', async () => {
		return new Response(JSON.stringify(FAKE_APP), {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	});

	router.get('/api/v1/pleroma/apps', async (c: Context): Promise<Response> => {
		return new Response(JSON.stringify({
			...FAKE_APP,
			redirect_uri: '',
		}), {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	});
}

export default appRouter;
