import { Context, Hono } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import { getPayloadFromContext } from '../../index.ts';
import { config } from 'https://deno.land/x/dotenv/mod.ts';
import neo4j from '../../neo4j.ts';
import { Note } from '../../schemas/activitypub.ts';
import { z } from 'https://deno.land/x/zod@v3.21.4/mod.ts';
import { transformActorToAccount } from '../../functions/transformActorToAccount.ts';
import { formatMastodonStatus } from '../../functions/formatMastodonStatus.ts';
import { transformStatusToNote } from '../../functions/transformStatusToNote.ts';
import { findOrCreateActor } from '../../functions/findOrCreateActor.ts';
import uuid62 from 'npm:uuid62';
await config({export: true});
const driver = neo4j;

const pollPayloadSchema = z.object({
	options: z.array(z.string()).nonempty(),
	expires_in: z.number().positive(),
	multiple: z.boolean().optional(),
	hide_totals: z.boolean().optional(),
});

const statusPayloadSchema = z.object({
	status: z.string(),
	in_reply_to_id: z.string().nullable(),
	quote_id: z.string().nullable(),
	media_ids: z.array(z.string()),
	sensitive: z.boolean(),
	spoiler_text: z.string(),
	visibility: z.string(),
	content_type: z.string(),
	poll: pollPayloadSchema.nullable(),
	scheduled_at: z.string().nullable(),
	to: z.array(z.string()),
	group_id: z.string().nullable().optional(),
});


export const get_status_route = async (c: Context) => {
	try {
		const webScheme = Deno.env.get('WEB_SCHEME');
		const webDomain = Deno.env.get('WEB_DOMAIN');
		const username = getPayloadFromContext(c.req).user.username;

		const statusId = c.req.param('id');

		const status = await getStatusById(statusId);



		return new Response(JSON.stringify(await formatMastodonStatus(status, username)), {
			headers: { 'Content-Type': 'application/json' },
		});
	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), {
			status: 404,
			headers: { 'Content-Type': 'application/json' },
		});
	}
}
export const bookmark_status_route = async (c: Context) => {
	try {
		const statusId = c.req.param('id');
		const username = getPayloadFromContext(c.req).user.username;

		const status = await getStatusById(statusId);

		if (!status) {
			throw new Error('Status not found');
		}

		// If the status is not already bookmarked by the user, add it to the user's bookmarks
		if(!status.bookmarked) {
			await addStatusToBookmarks(username, statusId);
			status.bookmarked = true;
		}

		return new Response(JSON.stringify(await formatMastodonStatus(status, username)), {
			headers: { 'Content-Type': 'application/json' },
		});

	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), {
			status: 500,
			headers: { 'Content-Type': 'application/json' },
		});
	}
};

export const favourite_status_route = async (c: Context) => {
	try {
		const statusId = c.req.param('id');
		const username = getPayloadFromContext(c.req).user.username;

		const status = await getStatusById(statusId);

		if (!status) {
			throw new Error('Status not found');
		}

		// If the status is not already favourited by the user, add it to the user's favourites
		if(!status.favourited) {
			await addStatusToFavourites(username, statusId);
			status.favourited = true;
		}

		return new Response(JSON.stringify(await formatMastodonStatus(status, username)), {
			headers: { 'Content-Type': 'application/json' },
		});

	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), {
			status: 500,
			headers: { 'Content-Type': 'application/json' },
		});
	}
}
export const create_status_route = async (c: Context) => {
	try {
		const session = driver.session();
		const data = await c.req.json();
		const username = getPayloadFromContext(c.req).user.username;

		const actor = await findOrCreateActor(username);

		let status = parseAndValidatePayload(data);

		let pollId = null;
		if (status.poll !== null) {
			pollId = await createPoll(session, status.poll, username);
		}

		const note = await transformStatusToNote(status, username);

		await createNote(note, pollId, session);
		note.account = transformActorToAccount(actor);

		await session.close();

		return new Response(JSON.stringify(await formatMastodonStatus(note, username)), {
			headers: { 'Content-Type': 'application/json' },
		});
	} catch (error) {
		console.log(error);
		return new Response(JSON.stringify({ error: error.message }), {
			status: 500,
			headers: { 'Content-Type': 'application/json' },
		});
	}
};
export const status_context_route = async (c: Context) => {
	try {
		const statusId = c.req.param('id');
		const username = getPayloadFromContext(c.req).user.username;

		// Fetch the status by id
		const status = await getStatusById(statusId);
		if (!status) {
			throw new Error('Status not found');
		}

		// Fetch ancestors and descendants of the status
		const ancestors = await getStatusAncestors(statusId, username);
		const descendants = await getStatusDescendants(statusId, username);

		// Prepare the response object
		const responseObj: any = {};

		// Include ancestors and descendants only if they are not empty
		if (ancestors.length > 0) {
			responseObj.ancestors = ancestors;
		} else {
			responseObj.ancestors = [];
		}
		if (descendants.length > 0) {
			responseObj.descendants = descendants;
		} else {
			responseObj.descendants = [];
		}

		// Return the status context
		return new Response(JSON.stringify(responseObj), {
			headers: { 'Content-Type': 'application/json' },
		});
	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), {
			status: 500,
			headers: { 'Content-Type': 'application/json' },
		});
	}
}

export const delete_status_route = async (c: Context) => {
	try {
		const statusId = c.req.param('id');
		const username = getPayloadFromContext(c.req).user.username;

		const status = await getStatusById(statusId);

		if (!status || !status.properties.attributedTo.includes(username)) {
			throw new Error('Status not found or user unauthorized');
		}

		await deleteStatusById(statusId);

		return new Response(JSON.stringify(await formatMastodonStatus(status, username)), {
			headers: { 'Content-Type': 'application/json' },
		});

	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), {
			status: 500,
			headers: { 'Content-Type': 'application/json' },
		});
	}
}

const getStatusById = async (statusId: string) => {
	const session = driver.session();

	try {
		const result = await session.run(
			"MATCH (a:Actor)-[:CREATED]->(n:Note) WHERE n.id ENDS WITH $statusId RETURN a, n",
			{ statusId }
		);

		if (result.records.length === 0) {
			throw new Error('Status not found');
		}

		const record = result.records[0];
		const note = record.get('n');
		const actor = record.get('a');
		note.account = actor;

		return note;
	} finally {
		await session.close();
	}
}

const createPoll = async (session: any, poll: any, username: string) => {
	const pollCreationResult = await session.run(
		"CREATE (p:Poll {objectId: $objectId, options: $options, expires_in: $expires_in, multiple: $multiple, hide_totals: $hide_totals, creator: $username, created_at: $created_at}) RETURN ID(p)",
		{
			objectId: uuid62.v4(),
			options: poll.options,
			expires_in: poll.expires_in,
			multiple: poll.multiple || false,
			hide_totals: poll.hide_totals || false,
			username,
			created_at: new Date().toISOString() // current timestamp in ISO string format
		}
	);
	return pollCreationResult.records[0].get('ID(p)').toString();
}

const parseAndValidatePayload = (data: any) => {
	const result = statusPayloadSchema.safeParse(data);
	if (!result.success) {
		throw new Error('Invalid data');
	}
	return result.data;
}


const createNote = async (note: Note, pollId: string | null = null, session: any) => {
	let inReplyToAuthor = null;
	const actorId = note.attributedTo;
	note.objectId = uuid62.v4();

	try {
		if (note.inReplyTo) {
			inReplyToAuthor = await getInReplyToAuthor(session, note.inReplyTo);
		}

		await createNoteInDatabase(session, note, actorId, inReplyToAuthor);
		if (note.inReplyTo) {
			await createReplyToRelationship(session, note);
		}
		if (pollId) {
			await createHasPollRelationship(session, note, pollId);
		}
		if (note.media_ids && Array.isArray(note.media_ids)) {
			await createHasMediaRelationships(session, note);
		}
	} catch (error: any) {
		console.log(error)
	}
}

const createNoteInDatabase = async (session: any, note: Note, actorId: string, inReplyToAuthor: string) => {
	let objectId = uuid62.v4();
	let noteCreationQuery = "";
	if (note.inReplyTo) {
		noteCreationQuery = "MATCH (a:Actor {id: $actorId}) " +
			"CREATE (a)-[:CREATED]->(n:Note {id: $noteId, objectId: $objectId, content: $content, published: $published, attributedTo: $attributedTo, inReplyTo: $inReplyTo, inReplyToAuthor: $inReplyToAuthor})";
	} else {
		noteCreationQuery = "MATCH (a:Actor {id: $actorId}) " +
			"CREATE (a)-[:CREATED]->(n:Note {id: $noteId, objectId: $objectId, content: $content, published: $published, attributedTo: $attributedTo})";
	}

	await session.run(noteCreationQuery, {
		noteId: note.id,
		objectId: new URL(note.id).pathname.split('/').pop(),
		content: note.content,
		published: note.published,
		attributedTo: note.attributedTo,
		inReplyTo: note.inReplyTo,
		actorId,
		inReplyToAuthor
	});
}

const createReplyToRelationship = async (session: any, note: Note) => {
	await session.run(
		"MATCH (n:Note {id: $noteId}), (m:Note) WHERE m.id ENDS WITH $inReplyToId CREATE (n)-[:REPLY_TO]->(m)",
		{
			noteId: note.id,
			inReplyToId: note.inReplyTo.split("/").pop(),
		}
	);
}

const createHasPollRelationship = async (session: any, note: Note, pollId: string) => {
	await session.run(
		"MATCH (n:Note {id: $noteId}), (p:Poll) WHERE ID(p) = $pollId CREATE (n)-[:HAS_POLL]->(p)",
		{
			noteId: note.id,
			pollId: parseInt(pollId),
		}
	);
}

const createHasMediaRelationships = async (session: any, note: Note) => {
	for (const mediaId of note.media_ids) {
		await session.run(
			"MATCH (n:Note {id: $noteId}), (m:Media {id: $mediaId}) CREATE (n)-[:HAS_MEDIA]->(m)",
			{
				noteId: note.id,
				mediaId: mediaId,
			}
		);
	}
}

const deleteStatusById = async (statusId: string) => {
	const session = driver.session();

	try {
		await session.run(
			"MATCH (n:Note) WHERE n.id ENDS WITH $statusId DETACH DELETE n",
			{ statusId }
		);

	} finally {
		await session.close();
	}
}

const addStatusToBookmarks = async (username: string, statusId: string) => {
	const session = driver.session();
	let result;
	try {
		result = await session.writeTransaction(txc =>
			txc.run(
				"MATCH (a:Actor {preferredUsername: $username}), (n:Note) " +
				"WHERE n.id ENDS WITH $statusId " +
				"MERGE (a)-[:BOOKMARKED]->(n) RETURN a,n",
				{ username, statusId }
			)
		);

		if (result.records.length === 0) {
			throw new Error('No nodes found to create a relationship');
		}
	} catch (error) {
		console.log(error);
	} finally {
		await session.close();
	}
	return result;
}

const addStatusToFavourites = async (username: string, statusId: string) => {
	const session = driver.session();
	let result;
	try {
		result = await session.writeTransaction(txc =>
			txc.run(
				"MATCH (a:Actor { preferredUsername: $username }), (n:Note) " +
				"WHERE n.id ENDS WITH $statusId " +
				"MERGE (a)-[:FAVOURITED]->(n) RETURN a,n",
				{ username, statusId }
			)
		);

		if (result.records.length === 0) {
			throw new Error('No nodes found to create a relationship');
		}
	} catch (error) {
		console.log(error);
	} finally {
		await session.close();
	}
	return result;
}

const getStatusAncestors = async (statusId: string, username: string) => {
	const session = driver.session();

	try {
		const result = await session.run(
			"MATCH (n:Note)-[r:REPLY_TO*]->(ancestor:Note) WHERE n.id ENDS WITH $statusId RETURN ancestor",
			{ statusId }
		);

		const ancestors = await Promise.all(result.records.map(async record => await formatMastodonStatus(record.get('ancestor').properties, username)));
		return ancestors;
	} finally {
		await session.close();
	}
}

const getStatusDescendants = async (statusId: string, username: string) => {
	const session = driver.session();

	try {
		const result = await session.run(
			"MATCH (n:Note)<-[r:REPLY_TO*]-(descendant:Note) WHERE n.id ENDS WITH $statusId RETURN descendant",
			{ statusId }
		);

		const descendants = await Promise.all(result.records.map(async record => await formatMastodonStatus(record.get('descendant').properties, username)));
		return descendants;
	} finally {
		await session.close();
	}
}

const getInReplyToAuthor = async (session: any, inReplyTo: string) => {
	const result = await session.run(
		"MATCH (n:Note) WHERE n.id ENDS WITH $inReplyToId RETURN n.attributedTo as inReplyToAuthor",
		{
			inReplyToId: inReplyTo.split("/").pop(),
		}
	);
	return result.records[0]?.get('inReplyToAuthor');
}

