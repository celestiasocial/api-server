import { getPayloadFromContext } from "../../index.ts";
import neo4jDriver from "../../neo4j.ts";
import { createRelationshipResponse } from "./createRelationshipResponse.ts";
const driver = neo4jDriver;

export const mute_account_route = async (c: Context) => {
	try {
		// Extract the current user's ID and the account ID to be muted
		const currentUserId = await getPayloadFromContext(c.req).user.id;
		const urlParamId = c.req.param('id');

		// Extract query parameters
		const notifications = c.req.query('notifications') || true;
		const duration = c.req.query('duration') || 0;
		const expiresIn = c.req.query('expires_in') || 0;

		// Use duration if provided, else use expires_in
		const muteDuration = duration || expiresIn;

		// Call a function to mute the account
		// Note: You would need to implement this function based on your specific needs
		const mute = muteAccount(currentUserId, urlParamId, notifications, muteDuration);

		// Return the mute status
		return createRelationshipResponse(urlParamId, currentUserId)
	} catch (error) {
		return new Response(JSON.stringify({ error: error.message }), {
			status: 500,
			headers: { 'Content-Type': 'application/json' },
		});
	}
};

const muteAccount = async (currentUserId: string, urlParamId: string, notifications: boolean, muteDuration: number) => {
	const session = driver.session();
	let result;
	try {
		result = await session.writeTransaction(txc =>
			txc.run(
				"MATCH (a:Actor {objectId: $currentUserId}), (b:Actor {objectId: $urlParamId}) " +
				"MERGE (a)-[r:MUTED {notifications: $notifications, duration: $muteDuration}]->(b) " +
				"RETURN r",
				{ currentUserId, urlParamId, notifications, muteDuration }
			)
		);

		if (result.records.length === 0) {
			throw new Error('No nodes found to create a relationship');
		}
	} catch (error) {
		console.log(error);
	} finally {
		await session.close();
	}
	return result.records[0].get('r').properties;
};
