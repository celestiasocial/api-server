import { Hono, Context } from 'https://deno.land/x/hono@v3.3.4/mod.ts';
import { getActorFromContext } from '../../index.ts';
import { load } from 'https://deno.land/std@0.196.0/dotenv/mod.ts';
const env = await load();
import neo4j from '../../neo4j.ts';
import { transformActorToAccount } from '../../functions/transformActorToAccount.ts';
import { findActorByUsername } from '../../functions/findActorByUsername.ts';
import { formatMastodonStatus } from "../../functions/formatMastodonStatus.ts";
const driver = neo4j;
export const account_lookup_route = async (c: Context) => {
		try {
			const username = c.req.query('acct');

			if (!username) {
				return createUserNotFoundError();
			}

			const actor = await findActorByUsername(username);

			if (!actor) {
				return createUserNotFoundError();
			}

			return createJSONResponse(transformActorToAccount(actor), 200);

		} catch(error) {
			return createJSONResponse({ error: error.message }, 500);
		}
};

export const get_statuses_by_account_id_route = async (c: Context) => {
		try {
			const session = driver.session();
			const accountIdOrNickname = c.req.param('id');
			const currentUser = getActorFromContext(c.req);

			// Fetch all notes related to the user
			const result = await session.run(
				"MATCH (a:Actor)-[:CREATED]->(n:Note) " +
				"WHERE (a.objectId = $accountIdOrNickname OR a.preferredUsername = $accountIdOrNickname) " +
				"RETURN n " +
				"ORDER BY n.published DESC",
				{ accountIdOrNickname }
			);

			// Parse and validate query parameters
			const max_id = c.req.query('max_id') || null;
			const min_id = c.req.query('min_id') || null;
			const since_id = c.req.query('since_id') || null;
			const offset = Number(c.req.query('offset')) || 0;
			const limit = Math.min(Number(c.req.query('limit')) || 20, 40);
			const pinned = c.req.query('pinned') === 'true';
			const tagged = c.req.query('tagged') || null;
			const only_media = c.req.query('only_media') === 'true';
			const exclude_replies = c.req.query('exclude_replies') === 'true';
			const exclude_reblogs = c.req.query('exclude_reblogs') === 'true';
			const exclude_visibilities = c.req.query('exclude_visibilities') || [];
			const include_muted = c.req.query('include_muted') === 'true';
			const include_muted_reactions = c.req.query('include_muted_reactions') === 'true';

			// Get all notes
			let notes = result.records.map(record => record.get('n').properties);

			// Apply filters
			notes = filterPinned(notes, pinned);
			notes = filterTagged(notes, tagged);
			notes = filterOnlyMedia(notes, only_media);
			notes = filterExcludeReplies(notes, exclude_replies);
			notes = filterExcludeReblogs(notes, exclude_reblogs);
			notes = filterExcludeVisibilities(notes, exclude_visibilities);
			notes = filterIncludeMuted(notes, include_muted);
			notes = filterIncludeMutedReactions(notes, include_muted_reactions);

			// Apply pagination
			notes = notes.slice(offset, offset + limit);

			// Prepare the response
			const statuses = await Promise.all(notes.map(async note => {
				return await formatMastodonStatus(note, currentUser);
			}));

			return new Response(JSON.stringify(statuses), {
				headers: { 'Content-Type': 'application/json' },
			});
		} catch (error) {
			return new Response(JSON.stringify({ error: error.message }), {
				status: 500,
				headers: { 'Content-Type': 'application/json' },
			});
		}
};

const filterPinned = (notes, pinned) => {
	if (!pinned) {
		return notes;
	}
	return notes.filter(note => note.isPinned === true);
};

const filterTagged = (notes, tagged) => {
	if (!tagged) {
		return notes;
	}
	return notes.filter(note => note.tag === tagged);
};

const filterOnlyMedia = (notes, onlyMedia) => {
	if (!onlyMedia) {
		return notes;
	}
	return notes.filter(note => note.hasMedia === true);
};

const filterExcludeReplies = (notes, excludeReplies) => {
	if (!excludeReplies) {
		return notes;
	}
	return notes.filter(note => note.isReply !== true);
};

const filterExcludeReblogs = (notes, excludeReblogs) => {
	if (!excludeReblogs) {
		return notes;
	}
	return notes.filter(note => note.isReblog !== true);
};

const filterExcludeVisibilities = (notes, excludeVisibilities) => {
	if (!excludeVisibilities.length) {
		return notes;
	}
	return notes.filter(note => note.visibility && !excludeVisibilities.includes(note.visibility));
};

const filterIncludeMuted = (notes, includeMuted) => {
	if (!includeMuted) {
		return notes;
	}
	return notes.filter(note => note.isMuted === true);
};

const filterIncludeMutedReactions = (notes, includeMutedReactions) => {
	if (!includeMutedReactions) {
		return notes;
	}
	return notes.filter(note => note.hasMutedReactions === true);
};

// Utility function to create a JSON response
const createJSONResponse = (data: any, status: number): Response => {
	return new Response(JSON.stringify(data), {
		status,
		headers: { 'Content-Type': 'application/json' },
	});
};

// Utility function to create a User Not Found error response
const createUserNotFoundError = (): Response => {
	return createJSONResponse({ error: 'User not found' }, 404);
};
