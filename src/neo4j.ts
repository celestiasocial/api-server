import { load } from "https://deno.land/std@0.196.0/dotenv/mod.ts";
import neo4j from "https://deno.land/x/neo4j_lite_client@4.4.1-preview2/mod.ts";
const env = await load();

// Create the Neo4j driver instance
const neo4jDriver = neo4j.driver(
	env["NEO4J_URL"],
	neo4j.auth.basic(env["NEO4J_USER"], env["NEO4J_PASS"]),
	{
		maxConnectionLifetime: 30 * 60 * 1000,
		connectionAcquisitionTimeout: 2 * 60 * 1000,
	}
);

// Export the Neo4j driver instance
export default neo4jDriver;
